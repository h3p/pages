'use strict';

/**
 * Polyfills
 */
let copyToClipboard;

if (!Array.prototype.includes) {
    Array.prototype.includes = function () {
        let index;

        for (index of arguments) {
            if (this.indexOf(index) === -1) {
                return false;
            }
        }

        return true;
    }
}

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;

        for (let i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

if (!NodeList.prototype[Symbol.iterator]) {
    NodeList.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];
}

if (!Object.values) {
    Object.values = function (item) {
        return Object.keys(item).reduce(function (result, key) {result.push(item[key]); return result;}, []);
    };
}

if (navigator.clipboard && navigator.clipboard.writeText) {
    copyToClipboard = function (textToCopy) {
        navigator.clipboard.writeText(textToCopy).then();
    }
} else {
    copyToClipboard = function (textToCopy) {
        let copyBuffer = document.createElement('TEXTAREA');

        copyBuffer.value = textToCopy;

        copyBuffer.style.border     = 'none';
        copyBuffer.style.background = 'transparent';
        copyBuffer.style.color      = 'transparent';
        copyBuffer.style.position   = 'absolute';
        copyBuffer.style.left       = '0';
        copyBuffer.style.top        = '0';
        copyBuffer.style.resize     = 'none';
        copyBuffer.style.overflow   = 'visible';

        document.body.appendChild(copyBuffer);

        copyBuffer.focus();
        copyBuffer.select();

        document.execCommand('copy');

        copyBuffer.remove();
    }
}

/**
 * Store for used heroes and towns divided by modes
 *
 * @type {{
 * townPrefix: string,
 * heroPrefix: string,
 * usedHeroes: {object},
 * usedTowns: {object},
 * separator: string,
 * init: (function()),
 * resetUsedHeroes: (function(string, string)),
 * resetUsedTowns: (function(string)),
 * getUsedTowns: (function(string)),
 * getUsedHeroes: (function(string): {object}),
 * addUsedTown: (function(string, string)),
 * addUsedHero: (function(string, string, string)),
 * persistUsedTowns: (function(string)),
 * persistUsedHeroes: (function(string, string))
 * }}
 */
const store = {
    townPrefix: 'u-towns_',
    heroPrefix: 'u-heroes_',
    separator : '@',
    usedTowns : {},
    usedHeroes: {},
    /**
     * Prepare object for its operate
     */
    init: function () {
        let index, mode, town, heroIndex;

        // read store from local storage of the browser
        for (index in localStorage)
            if (localStorage.hasOwnProperty(index)) {
                if (index.startsWith(this.townPrefix)) {
                    this.usedTowns[index.replace(this.townPrefix, '')] = JSON.parse(localStorage.getItem(index));
                } else if (index.startsWith(this.heroPrefix)) {
                    heroIndex = index.replace(this.heroPrefix, '').split(this.separator, 2);
                    town      = heroIndex.pop();
                    mode      = heroIndex.pop();

                    if (!this.usedHeroes[mode]) {
                        this.usedHeroes[mode] = {};
                    }

                    if (!this.usedHeroes[mode][town]) {
                        this.usedHeroes[mode][town] = {};
                    }

                    this.usedHeroes[mode][town] = JSON.parse(localStorage.getItem(index));
                }
            }
    },
    /**
     * Get copy of used heroes object
     *
     * @param {string} mode mode unique id
     *
     * @returns {object}
     */
    getUsedHeroes: function (mode) {
        let copy = {},
            index;

        if (this.usedHeroes[mode]) {
            for (index of Object.keys(this.usedHeroes[mode])) {
                copy[index] = this.usedHeroes[mode][index].slice();
            }
        }

        return copy;
    },
    /**
     * Safely add used hero
     *
     * @param {string} mode mode unique id
     * @param {string} town town name
     * @param {string} hero hero name
     */
    addUsedHero: function (mode, town, hero) {
        if (!this.usedHeroes[mode]) {
            this.usedHeroes[mode] = {};
        }

        if (!this.usedHeroes[mode][town]) {
            this.usedHeroes[mode][town] = [];
        }

        this.usedHeroes[mode][town].push(hero);
    },
    /**
     * Reset used heroes of specific town
     *
     * @param {string} mode mode unique id
     * @param {string} town town name
     */
    resetUsedHeroes: function (mode, town) {
        this.usedHeroes[mode][town] = [];
    },
    /**
     * Save specific town used heroes in Local Storage
     *
     * @param {string} mode mode unique id
     * @param {string} town town name
     */
    persistUsedHeroes: function (mode, town) {
        if (!this.usedHeroes.hasOwnProperty(mode)) {
            throw 'Unknown mode ID';
        }

        if (!this.usedHeroes[mode].hasOwnProperty(town)) {
            throw 'Unknown town ID';
        }

        localStorage.setItem(this.heroPrefix + mode + this.separator + town, JSON.stringify(this.usedHeroes[mode][town]))
    },
    /**
     * Get copy of used towns for specific mode
     *
     * @param {string} mode mode unique id
     *
     * @returns {string[]}
     */
    getUsedTowns: function (mode) {
        return this.usedTowns[mode] ? this.usedTowns[mode].slice() : [];
    },
    /**
     * Safely add used town
     *
     * @param {string} mode mode unique id
     * @param {string} town town name
     */
    addUsedTown: function (mode, town) {
        if (!this.usedTowns[mode]) {
            this.usedTowns[mode] = [];
        }

        this.usedTowns[mode].push(town);
    },
    /**
     * Reset used towns
     *
     * @param {string} mode mode unique id
     */
    resetUsedTowns: function (mode) {
        this.usedTowns[mode] = [];
    },
    /**
     * Save used towns in Local Storage
     *
     * @param {string} mode mode unique id
     */
    persistUsedTowns: function (mode) {
        if (!this.usedTowns.hasOwnProperty(mode)) {
            throw 'Unknown mode ID';
        }

        localStorage.setItem(this.townPrefix + mode, JSON.stringify(this.usedTowns[mode]));
    }
};

/**
 * Store for mode providing
 *
 * @type {{
 * name: string,
 * modes: null|Object,
 * init: (function()),
 * addMode: (function(string, string|null=, string[]|null=, string[]|null=, boolean|null=, boolean|null=)),
 * updateCustomMode: (function(string, string|null=, string[]|null=, string[]|null=, boolean|null)),
 * getCustomMode: (function(string): object),
 * getRandomId: (function(): string),
 * removeMode: (function(string)),
 * isMode: (function(string): boolean),
 * getListOfModes: (function(): string[]),
 * getName: (function(string): string),
 * getIsMirror: (function(string): boolean|null),
 * getAllowedHeroes: (function(string, Object): {}),
 * getAllowedTowns: (function(string, string[]): string[]),
 * persist: (function())
 * }}
 */
const modeProvider = {
    modes: null,
    name : 'randomizer_cMode',
    /**
     * Prepare object for its operate
     */
    init: function () {
        let modes = localStorage.getItem(this.name),
            index;

        this.modes = {};

        if (modes) {
            modes = JSON.parse(modes);

            for (index in modes)
                if (modes.hasOwnProperty(index)) {
                    this.addMode(index, modes[index].name, modes[index].bannedHeroes, modes[index].bannedTowns, modes[index].isMirror, false);
                }
        }
    },
    /**
     * Add new mode with all required fields
     *
     * @param {string}        id             mode unique id
     * @param {string|null}   [name]         mode name, if not set id will be used instead
     * @param {string[]|null} [bannedHeroes] array of banned heroes, only names
     * @param {string[]|null} [bannedTowns]  array of banned towns, only names
     * @param {boolean|null}  [isMirror]     is this mode mirror, by default false
     * @param {boolean|null}  [system]       is this mode system, by default false
     */
    addMode: function (id, name, bannedHeroes, bannedTowns, isMirror, system) {
        if (this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' already is registered';
        }

        bannedHeroes = bannedHeroes || [];
        bannedTowns  = bannedTowns || [];
        system       = !!system;
        isMirror     = !!isMirror;
        name         = name || id;

        this.modes[id] = {name, bannedHeroes, bannedTowns, isMirror, system};
    },
    /**
     * Update parameters of specific non system mode
     *
     * @param {string}        id             mode unique id
     * @param {string|null}   [name]         mode name
     * @param {string[]|null} [bannedHeroes] array of banned heroes, only names
     * @param {string[]|null} [bannedTowns]  array of banned towns, only names
     * @param {boolean|null}  [isMirror]     is this mode mirror
     */
    updateCustomMode: function (id, name, bannedHeroes, bannedTowns, isMirror) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        } else if (this.modes[id].system) {
            throw 'Mode with id ' + id + ' is system';
        }

        if (name && this.modes[id].name !== name) {
            this.modes[id].name = name;
        }

        if (typeof isMirror === "boolean" && this.modes[id].isMirror !== isMirror) {
            this.modes[id].isMirror = isMirror;
        }

        if (Array.isArray(bannedHeroes)) {
            this.modes[id].bannedHeroes = this.modes[id].bannedHeroes.reduce(function (result, item) {
                if (bannedHeroes.includes(item)) {
                    result.push(item);
                    bannedHeroes.splice(bannedHeroes.indexOf(item), 1)
                }

                return result;
            }, []).concat(bannedHeroes);
        }

        if (Array.isArray(bannedTowns)) {
            this.modes[id].bannedTowns = this.modes[id].bannedTowns.reduce(function (result, item) {
                if (bannedTowns.includes(item)) {
                    result.push(item);
                    bannedTowns.splice(bannedTowns.indexOf(item), 1)
                }

                return result;
            }, []).concat(bannedTowns);
        }
    },
    /**
     * Get random id for a new custom mode
     *
     * @return {string}
     */
    getRandomId: function () {
        let id;

        do {
            id = 'm-' + Math.floor(Math.random() * 10000).toString().padStart(4, '0');
        } while (this.modes.hasOwnProperty(id));

        return id;
    },
    /**
     * Get copy of custom mode
     *
     * @param {string} id custom mode unique id
     *
     * @return {object}
     */
    getCustomMode   : function (id) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        } else if (this.modes[id].system) {
            throw 'Mode with id ' + id + ' is system';
        }

        return {
            name        : this.modes[id].name,
            bannedHeroes: this.modes[id].bannedHeroes.slice(),
            bannedTowns : this.modes[id].bannedTowns.slice(),
            mirror      : this.modes[id].isMirror
        }
    },
    /**
     * Get allowed by specific mode list of towns
     *
     * @param {string}   id       mode unique id
     * @param {string[]} townList array of towns, only names
     *
     * @returns {string[]}
     */
    getAllowedTowns: function (id, townList) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        }

        return this.modes[id].bannedTowns.reduce(function (result, item) {
            if (result.includes(item)) {
                result.splice(result.indexOf(item), 1);
            }

            return result;
        }, townList);
    },
    /**
     * Get allowed by specific mode list of heroes
     *
     * @param {string}   id                mode unique id
     * @param {Object}   heroList          object with heroes
     * @param {string[]} heroList[].heroes list of heroes in town
     *
     * @returns {string[]}
     */
    getAllowedHeroes: function (id, heroList) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        }

        let result = {},
            index;

        for (index in heroList)
            if (heroList.hasOwnProperty(index)) {
                if (this.modes[id].bannedTowns.includes(index)) {
                    continue;
                }

                result[index] = this.modes[id].bannedHeroes.reduce(function (result, item) {
                    if (result.includes(item)) {
                        result.splice(result.indexOf(item), 1);
                    }

                    return result;
                }, Object.keys(heroList[index].heroes));
            }

        return result;
    },
    /**
     * Check if specific mode is mirrored
     *
     * @param {string} id mode unique id
     *
     * @returns {boolean|null}
     */
    getIsMirror: function (id) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        }

        return this.modes[id].isMirror;
    },
    /**
     * Safely remove mode
     *
     * @param {string} id mode unique id
     */
    removeMode: function (id) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        }

        delete this.modes[id];
    },
    /**
     * Get name of specific mode
     *
     * @param {string} id mode unique id
     *
     * @returns {string}
     */
    getName: function (id) {
        if (!this.modes.hasOwnProperty(id)) {
            throw 'Mode with id ' + id + ' is not registered';
        }

        return this.modes[id].name;
    },
    /**
     * Get ids of registered modes
     *
     * @returns {string[]}
     */
    getListOfModes: function () {
        return Object.keys(this.modes);
    },
    /**
     * Check if mode exists
     *
     * @param {string} id mode unique id
     *
     * @returns {boolean}
     */
    isMode: function (id) {
        return this.modes.hasOwnProperty(id);
    },
    /**
     * Store custom modes in Local Storage
     */
    persist: function () {
        let modes = {},
            index;

        for (index in this.modes)
            if (this.modes.hasOwnProperty(index)) {
                if (this.modes[index].system) {
                    continue;
                }

                modes[index] = this.modes[index];
            }

        if (Object.getOwnPropertyNames(modes)) {
            localStorage.setItem(this.name, JSON.stringify(modes));
        }
    }
};

/**
 * Random picker with store of last pick
 *
 * @type {{
 * LAST_PICK_LENGTH: number,
 * name: string,
 * lastPick: null|{hero: object},
 * init: (function()),
 * getRandomHero: ((function(object, object, string[], string=): ({town: {string}, hero: {string}}|{town: {string}, hero: null}|{town: null, hero: null}))),
 * updateLastPick: (function(string, string)),
 * persistLastPick: (function())
 * }}
 */
const picker = {
    LAST_PICK_LENGTH: 2,
    name            : 'last_pick',
    lastPick        : null,
    /**
     * Prepare object for its operate
     */
    init: function () {
        let lastPick = localStorage.getItem(this.name);

        this.lastPick = lastPick ? JSON.parse(lastPick) : {hero: {}};
    },
    /**
     * Get random hero from heroes object filtered by used heroes and towns
     *
     * @param {object}   heroes     object of allowed heroes
     * @param {object}   usedHeroes array of used heroes, only names
     * @param {string[]} usedTowns  array of used towns, only names
     * @param {string}   [town]     force specific town pick (for mirror picks)
     *
     * @returns {{town: {string}, hero: {string}}|{town: {string}, hero: null}|{town: null, hero: null}}
     */
    getRandomHero: function (heroes, usedHeroes, usedTowns, town) {
        let hero = null,
            list;

        if (!town) {
            list = Object.keys(heroes).filter(function (item) {
                return !usedTowns.includes(item) && !this.includes(item);
            }, this.lastPick[town] || []);

            if (!list.length) {
                return {town, hero};
            }

            town = list[Math.floor(Math.random() * list.length)];
        }

        usedHeroes = usedHeroes[town] || [];

        list = heroes[town].filter(function (item) {
            return !usedHeroes.includes(item) && !this.includes(item);
        }, this.lastPick.hero[town] || []);

        if (!list.length) {
            return {town, hero};
        }

        hero = list[Math.floor(Math.random() * list.length)];

        return {town, hero};
    },
    /**
     * Update last picked town and hero
     *
     * @param {string} town hero name
     * @param {string} hero town name
     */
    updateLastPick: function (town, hero) {
        if (this.lastPick.town && this.lastPick.town.length) {
            this.lastPick.town.push(town);

            if (this.lastPick.town.length > this.LAST_PICK_LENGTH) {
                this.lastPick.town.shift();
            }
        } else {
            this.lastPick.town = [town];
        }

        if (this.lastPick.hero[town] && this.lastPick.hero[town].length) {
            this.lastPick.hero[town].push(hero);

            if (this.lastPick.hero[town].length > this.LAST_PICK_LENGTH) {
                this.lastPick.hero[town].shift();
            }
        } else {
            this.lastPick.hero[town] = [hero];
        }
    },
    /**
     * Save last pick in Local Storage
     */
    persistLastPick: function () {
        localStorage.setItem(this.name, JSON.stringify(this.lastPick));
    }
};

/**
 * Modal window open`n`close handler
 *
 * @type {{
 * MODAL_HIDE_TIMEOUT: number,
 * bodies: {},
 * overlay: null|Element,
 * header: null|Element,
 * active: null|Element,
 * wrap: null|Element,
 * closeCallback: null|number,
 * init: (function()),
 * getActiveId: (function(): string|null),
 * open: (function(string=, string)),
 * switchTo: (function(string=, string)),
 * close: (function())
 * }}
 */
const modal = {
    MODAL_HIDE_TIMEOUT: 750,
    overlay           : null,
    header            : null,
    wrap              : null,
    active            : null,
    bodies            : {},
    closeCallback     : null,
    /**
     * Prepare object for its operate
     */
    init: function () {
        let bodies = document.querySelectorAll('.modal__content'),
            index;

        // save links for main DOM elements
        this.overlay = document.querySelector('#overlay');
        this.wrap    = document.querySelector('#content_wrap');
        this.header  = this.overlay.querySelector('#modal_header');

        for (index in bodies)
            if (bodies.hasOwnProperty(index)) {
                this.bodies[bodies[index].id] = bodies[index];
            }
    },
    /**
     * Show modal window and its specific by id
     *
     * @param {string} [header] modal header text
     * @param {string} id       modal container id which should be shown
     */
    open: function (header, id) {
        if (!this.bodies.hasOwnProperty(id)) {
            throw 'Unable to open modal with id ' + id;
        }

        // if there is close callback id - previous modal is still closing, and we need to prevent it to prevent errors
        if (this.closeCallback) {
            clearTimeout(this.closeCallback);

            this.closeCallback = null;
        }

        // disable scroll for main body
        document.body.classList.add('scroll__hidden');

        // display modal content by its id
        this.active = id;
        this.overlay.classList.remove('hidden');
        this.bodies[id].classList.remove('hidden');

        // write modal header
        this.header.innerHTML = header || '';

        // toggle CSS animated classes
        this.overlay.classList.remove('to-transparent');
        this.overlay.classList.add('to-visible');

        this.wrap.classList.remove('to-normal_color');
        this.wrap.classList.add('to-mono_chrome');

    },
    /**
     * Hide modal window and its content
     */
    close: function () {
        // if there is close callback id - previous modal is still closing, and it is possible second click, so we need to skip it
        if (this.closeCallback) {
            return;
        }

        // enable scroll for main body
        document.body.classList.remove('scroll__hidden');

        // toggle CSS animated styles
        this.overlay.classList.remove('to-visible');
        this.overlay.classList.add('to-transparent');

        this.wrap.classList.remove('to-mono_chrome');
        this.wrap.classList.add('to-normal_color');

        // after CSS animation done - hide active modal content
        this.closeCallback = setTimeout(function () {
            this.overlay.classList.add('hidden');
            this.bodies[this.active].classList.add('hidden');

            this.active = null;
            this.closeCallback = null;
        }.bind(this), this.MODAL_HIDE_TIMEOUT);
    },
    /**
     * Hide current modal window and show another one
     *
     * @param {string} [header] modal header text
     * @param {string} id       modal container id which should be shown
     */
    switchTo: function (header, id) {
        // if there is close callback id - previous modal is closing, to prevent errors - skip it
        if (this.closeCallback) {
            return;
        }

        if (!this.bodies.hasOwnProperty(id)) {
            throw 'Unable to open modal with id ' + id;
        }

        // toggle CSS animated styles
        this.overlay.classList.remove('to-visible');
        this.overlay.classList.add('to-transparent');

        // after CSS animation done - reshow modal with new content
        this.closeCallback = setTimeout(function () {
            this.bodies[this.active].classList.add('hidden');
            this.bodies[id].classList.remove('hidden');

            // display modal content by its id
            this.active = id;

            // write modal header
            this.header.innerHTML = header || '';

            // toggle CSS animated classes
            this.overlay.classList.remove('to-transparent');
            this.overlay.classList.add('to-visible');

            this.closeCallback = null;
        }.bind(this), this.MODAL_HIDE_TIMEOUT);
    },
    /**
     * Get id of shown content in modal window
     *
     * @returns {null|string}|
     */
    getActiveId: function () {
        return this.active;
    }
};

/**
 * Language store
 *
 * @type {{
 * KEY_SEPARATOR: string,
 * active: null|string,
 * lang: {},
 * init: (function()),
 * flatScheme: (function(object, string=, object)),
 * addLang: (function(string, object)),
 * setActiveLang: (function(string)),
 * removeLang: (function(string)),
 * translate: (function(string, boolean=): string)
 * }}
 */
const trans = {
    KEY_SEPARATOR: '.',
    active       : null,
    lang         : {},
    /**
     * Prepare object for its operate
     */
    init: function () {
        // bind shortcut for translate function
        window.translate = this.translate.bind(this);
    },
    /**
     * Modify scheme multidimensional object to flat with dot-style keys
     *
     * @param {object}      scheme      original scheme or its child
     * @param {null|string} [parentKey] parent key prefix for child scheme
     * @param {object}      result      link to result scheme object (flatten)
     */
    flatScheme: function (scheme, parentKey, result) {
        let index, key;

        for (index in scheme)
            if (scheme.hasOwnProperty(index)) {
                key = parentKey + index;

                if (typeof scheme[index] === 'string') {
                    result[key] = scheme[index];
                } else {
                    this.flatScheme(scheme[index], key + this.KEY_SEPARATOR, result);
                }
            }
    },
    /**
     * Safely add language and flat its scheme
     *
     * @param {string} lang   language name
     * @param {object} scheme scheme object with translated phrases
     */
    addLang: function (lang, scheme) {
        let flattenScheme = {};

        lang = lang.toLowerCase();

        if (this.lang.hasOwnProperty(lang)) {
            throw 'Language scheme ' + lang + ' already registered in app';
        }

        this.flatScheme(scheme, '', flattenScheme);

        this.lang[lang] = flattenScheme;
    },
    /**
     * Set language which will be used for translation
     *
     * @param {string} lang language name
     */
    setActiveLang: function (lang) {
        lang = lang.toLowerCase();

        if (!this.lang.hasOwnProperty(lang)) {
            throw 'Language scheme ' + lang + ' is not registered in app';
        }

        this.active = lang;
    },
    /**
     * Safely remove language
     *
     * @param {string} lang language name
     */
    removeLang: function (lang) {
        lang = lang.toLowerCase();

        if (!this.lang.hasOwnProperty(lang)) {
            throw 'Language scheme ' + lang + ' is not registered in app';
        }

        delete this.lang[lang];
    },
    /**
     * Get translated phrase by its flat key, if there is no such phrase - key is returned, passing strict param as true will cause an exception in this case
     *
     * @param {string}  key flat phrase key
     * @param {boolean} [strict] strict mode, default: not strict
     *
     * @returns {string}
     */
    translate: function (key, strict) {
        strict = !!strict;

        if (!this.active && strict) {
            throw 'Active language does not set';
        } else if (!this.active) {
            return key;
        }

        if (this.lang[this.active].hasOwnProperty(key)) {
            return this.lang[this.active][key];
        } else if (!strict) {
            return key;
        }

        throw 'Required key ' + key + ' not found in scheme';
    }
};

/**
 * Small popup window handler
 *
 * @type {{
 * CONTAINER_ID: string,
 * WRAP_CLASS: string,
 * MESSAGE_CLASS: string,
 * BORDER_CLASS: string,
 * POPUP_TIMEOUT: number,
 * CONFIRM_BUTTON: string,
 * CONTROLS_CLASS: string,
 * messageElement: null|Element,
 * wrapElement: null|Element,
 * controls: null|Element,
 * body: null|Element,
 * closeHandler: null|function,
 * timeoutHandler: null|number,
 * confirmHandler: null|function,
 * cancelHandler: null|function,
 * init: (function()),
 * alert: (function(string)),
 * error: (function(string, null|string=)),
 * confirm: (function (string, null|function=, null|function=)),
 * hide: (function(Event|null))
 * }}
 */
const popup = {
    CONTAINER_ID  : 'popup-container',
    WRAP_CLASS    : 'popup__wrap',
    BORDER_CLASS  : 'popup__border',
    MESSAGE_CLASS : 'popup__message',
    CONFIRM_BUTTON: 'popup-confirm',
    CLOSE_BUTTON  : 'popup-close',
    CONTROLS_CLASS: 'popup-buttons',
    POPUP_TIMEOUT : 3000,
    wrapElement   : null,
    messageElement: null,
    controls      : null,
    body          : null,
    closeHandler  : null,
    timeoutHandler: null,
    confirmHandler: null,
    cancelHandler : null,
    /**
     * Prepare object for its operate
     */
    init: function () {
        let container     = document.createElement('DIV'),
            buttonConfirm = document.createElement('BUTTON'),
            buttonClose   = document.createElement('BUTTON');


        container.id        = this.CONTAINER_ID;
        this.body           = container.appendChild(document.createElement('DIV'));
        this.messageElement = this.body.appendChild(document.createElement('P'));
        this.wrapElement    = document.createElement('DIV');

        this.body.classList.add(this.BORDER_CLASS);
        this.wrapElement.classList.add(this.WRAP_CLASS);
        this.messageElement.classList.add(this.MESSAGE_CLASS);

        buttonConfirm.id = this.CONFIRM_BUTTON;
        buttonClose.id   = this.CLOSE_BUTTON;
        this.controls    = document.createElement('DIV');

        this.controls.classList.add(this.CONTROLS_CLASS);
        this.controls.appendChild(buttonClose);
        this.controls.appendChild(buttonConfirm);

        this.wrapElement.appendChild(container);

        this.closeHandler = this.hide.bind(this);
    },
    /**
     * Show message in popup which will disappear in a few seconds or user click anywhere
     *
     * @param {string} message message text to show
     */
    alert: function (message) {
        this.messageElement.innerHTML = '' + message;

        document.body.appendChild(this.wrapElement);

        document.body.addEventListener('click', this.closeHandler);

        this.timeoutHandler = setTimeout(function () {
            this.hide();
        }.bind(this), this.POPUP_TIMEOUT);
    },
    /**
     * Show error message in popup which will disappear if user click anywhere
     *
     * @param {string} message     message text to show
     * @param {string} [errorInfo] error info to show
     */
    error: function (message, errorInfo) {
        this.messageElement.innerHTML = '' + message;

        if (errorInfo) {
            this.messageElement.innerHTML += '<br>' + errorInfo;
        }

        document.body.appendChild(this.wrapElement);

        document.body.addEventListener('click', this.closeHandler);
    },
    /**
     * Show confirm message in popup which will disappear if user click anywhere or run callback if he clicks confirm button
     *
     * @param {string}   message         message text to show
     * @param {function} confirmCallback message text to show
     * @param {function} cancelCallback  message text to show
     */
    confirm: function (message, confirmCallback, cancelCallback) {
        this.messageElement.innerHTML = '' + message;

        this.confirmHandler = confirmCallback;
        this.cancelHandler  = cancelCallback;

        this.body.appendChild(this.controls);

        document.body.appendChild(this.wrapElement);

        document.body.addEventListener('click', this.closeHandler);
    }, /**
     * Hide and clear popup and remove event handler
     *
     * @param {Event|null} [e] event object
     */
    hide: function (e) {
        if (e) {
            e.stopPropagation();

            if (e.target.id === this.CONFIRM_BUTTON && this.confirmHandler) {
                this.confirmHandler();
            } else if (this.cancelHandler) {
                this.cancelHandler();
            }

            this.confirmHandler = null;
            this.cancelHandler  = null;
        }

        if (this.timeoutHandler) {
            clearTimeout(this.timeoutHandler);
        }

        if (this.controls.parentElement) {
            this.body.removeChild(this.controls);
        }

        document.body.removeChild(this.wrapElement);

        this.messageElement.innerHTML = '';

        document.body.removeEventListener('click', this.closeHandler);
    }
};

/**
 * Settings store
 *
 * @type {{
 * name: string,
 * isQueryString: null|boolean,
 * options: null|object,
 * toggleQueryStringStoreKey: (function()),
 * init: (function()),
 * setOption: (function(string, null|string|number|boolean)),
 * getOption: (function(string, null|any=): *),
 * getQueryStringStoreKey: (function(): null|boolean),
 * store: (function(): boolean)
 * }}
 */
const settings = {
    INT_PATTERN  : /^\d+$/,
    FLOAT_PATTERN: /^\d+\.\d+$/,
    name         : 'heroes_randomize',
    options      : null,
    isQueryString: null,
    /**
     * Prepare object for its operate
     */
    init: function () {
        let settings = localStorage.getItem(this.name),
            option, key, value;

        this.options = settings ? JSON.parse(settings) : {};

        if (location.search.length) {
            this.isQueryString = true;

            for (option of location.search.substring(1).split('&')) {
                option = option.split('=');
                value  = option.pop();
                key    = decodeURIComponent(option.pop());

                if (!this.options.hasOwnProperty(key)) {
                    value = decodeURIComponent(value);

                    switch (value) {
                        case 'true':
                            value = true;

                            break
                        case 'false':
                            value = false;

                            break
                        case 'null':
                        case 'undefined':
                            value = null;

                            break
                        default:
                            if (this.INT_PATTERN.test(value)) {
                                value = parseInt(value);
                            } else if (this.FLOAT_PATTERN.test(value)) {
                                value = parseFloat(value);
                            }
                    }

                    this.options[key] = value;
                }
            }
        } else {
            this.isQueryString = false;
        }
    },
    /**
     * Get value of specific option
     *
     * @param {string}   key            option name
     * @param {null|any} [defaultValue] default fallback if value of specific key is missing
     *
     * @returns {null|any}
     */
    getOption: function (key, defaultValue) {
        return this.options.hasOwnProperty(key) ? this.options[key] : defaultValue;
    },
    /**
     * Set specific option in store
     *
     * @param {string}                     key   option name
     * @param {null|boolean|string|number} value option value
     */
    setOption: function (key, value) {
        this.options[key] = value;
    },
    /**
     * Change query string save status
     */
    toggleQueryStringStoreKey: function () {
        this.isQueryString = !this.isQueryString;
    },
    /**
     * Get query string status
     *
     * @returns {null|boolean}
     */
    getQueryStringStoreKey: function () {
        return this.isQueryString;
    },
    /**
     * Save current parameters in Local Storage and if is required in Query String
     *
     * @return boolean
     */
    store: function () {
        let queryOptions = [],
            index;

        localStorage.setItem(this.name, JSON.stringify(this.options));

        if (this.isQueryString) {
            for (index in this.options)
                if (this.options.hasOwnProperty(index)) {
                    queryOptions.push(encodeURIComponent(index) + '=' + encodeURIComponent(this.options[index]));
                }

            if (queryOptions.length) {
                location.search = queryOptions.join('&');
            }

            return false;
        }

        return true;
    }
};
