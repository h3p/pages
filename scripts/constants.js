'use strict';

const URL_PATTERN = 'https://heroes.thelazy.net/index.php/';

const TOWNS_AND_HEROES = {
    Castle    : {
        img    : 'https://heroes.thelazy.net/images/b/bf/Town_portrait_Castle_small.png',
        heroes : {
            Christian  : {
                img      : 'https://heroes.thelazy.net/images/f/f8/Hero_Christian.png',
                type     : 'Knight',
                specialty: 'Ballista',
                skills   : ['Basic_Leadership', 'Basic_Artillery'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h00'
            },
            Edric      : {
                img      : 'https://heroes.thelazy.net/images/7/7f/Hero_Edric.png',
                type     : 'Knight',
                specialty: 'Griffins',
                skills   : ['Basic_Leadership', 'Basic_Armorer'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '2–3', unit: 'Griffin'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h01'
            },
            Orrin      : {
                img      : 'https://heroes.thelazy.net/images/4/4a/Hero_Orrin.png',
                type     : 'Knight',
                specialty: 'Archery',
                skills   : ['Basic_Leadership', 'Basic_Archery'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h02'
            },
            Sorsha     : {
                img      : 'https://heroes.thelazy.net/images/1/14/Hero_Sorsha.png',
                type     : 'Knight',
                specialty: 'Swordsmen',
                skills   : ['Basic_Leadership', 'Basic_Offense'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h03'
            },
            Sylvia     : {
                img      : 'https://heroes.thelazy.net/images/5/53/Hero_Sylvia.png',
                type     : 'Knight',
                specialty: 'Navigation',
                skills   : ['Basic_Leadership', 'Basic_Navigation'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h04'
            },
            Valeska    : {
                img      : 'https://heroes.thelazy.net/images/2/20/Hero_Valeska_%28HotA%29.png',
                type     : 'Knight',
                specialty: 'Archers',
                skills   : ['Basic_Leadership', 'Basic_Archery'],
                units    : [{count: '4–7', unit: 'Archer'}, {count: '4–7', unit: 'Archer'}, {count: '4–7', unit: 'Archer'}],
                shortId  : 'h05'
            },
            Tyris      : {
                img      : 'https://heroes.thelazy.net/images/c/c1/Hero_Tyris.png',
                type     : 'Knight',
                specialty: 'Cavaliers',
                skills   : ['Basic_Leadership', 'Basic_Tactics'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h06'
            },
            Lord_Haart : {
                img      : 'https://heroes.thelazy.net/images/6/6a/Hero_Lord_Haart.png',
                type     : 'Knight',
                specialty: 'Estates',
                skills   : ['Basic_Leadership', 'Basic_Estates'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h07'
            },
            Catherine  : {
                img      : 'https://heroes.thelazy.net/images/5/53/Hero_Catherine.png',
                type     : 'Knight',
                specialty: 'Swordsmen',
                skills   : ['Basic_Leadership', 'Basic_Offense'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h08'
            },
            Roland     : {
                img      : 'https://heroes.thelazy.net/images/7/7f/Hero_Roland.png',
                type     : 'Knight',
                specialty: 'Swordsmen',
                skills   : ['Basic_Leadership', 'Basic_Armorer'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h09'
            },
            Sir_Mullich: {
                img      : 'https://heroes.thelazy.net/images/9/9a/Hero_Sir_Mullich_%28HotA%29.png',
                type     : 'Knight',
                specialty: 'Speed',
                skills   : ['Advanced_Leadership'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0a'
            },
            Beatrice   : {
                img      : 'https://heroes.thelazy.net/images/0/05/Hero_Beatrice.png',
                type     : 'Knight',
                specialty: 'Scouting',
                skills   : ['Basic_Leadership', 'Basic_Scouting'],
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0b'
            },
            Adela      : {
                img      : 'https://heroes.thelazy.net/images/3/32/Hero_Adela.png',
                type     : 'Cleric',
                specialty: 'Bless',
                skills   : ['Basic_Wisdom', 'Basic_Diplomacy'],
                spell    : 'Bless',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0c'
            },
            Adelaide   : {
                img      : 'https://heroes.thelazy.net/images/b/b9/Hero_Adelaide_%28HotA%29.png',
                type     : 'Cleric',
                specialty: 'Frost_Ring',
                skills   : ['Advanced_Wisdom'],
                spell    : 'Frost_Ring',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0d'
            },
            Caitlin    : {
                img      : 'https://heroes.thelazy.net/images/5/50/Hero_Caitlin_%28HotA%29.png',
                type     : 'Cleric',
                specialty: 'Gold',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Cure',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0e'
            },
            Cuthbert   : {
                img      : 'https://heroes.thelazy.net/images/2/24/Hero_Cuthbert.png',
                type     : 'Cleric',
                specialty: 'Weakness',
                skills   : ['Basic_Wisdom', 'Basic_Estates'],
                spell    : 'Weakness',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0f'
            },
            Ingham     : {
                img      : 'https://heroes.thelazy.net/images/d/dd/Hero_Ingham.png',
                type     : 'Cleric',
                specialty: 'Monks',
                skills   : ['Basic_Wisdom', 'Basic_Mysticism'],
                spell    : 'Curse',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0g'
            },
            Loynis     : {
                img      : 'https://heroes.thelazy.net/images/8/86/Hero_Loynis.png',
                type     : 'Cleric',
                specialty: 'Prayer',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Prayer',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0h'
            },
            Rion       : {
                img      : 'https://heroes.thelazy.net/images/d/d4/Hero_Rion.png',
                type     : 'Cleric',
                specialty: 'First_Aid',
                skills   : ['Basic_Wisdom', 'Basic_First_Aid'],
                spell    : 'Stone_Skin',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0i'
            },
            Sanya      : {
                img      : 'https://heroes.thelazy.net/images/9/93/Hero_Sanya.png',
                type     : 'Cleric',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Dispel',
                units    : [{count: '10–20', unit: 'Pikeman'}, {count: '4–7', unit: 'Archer'}, {count: '2–3', unit: 'Griffin'}],
                shortId  : 'h0j'
            }
        },
        shortId: 't00'
    },
    Rampart   : {
        img    : 'https://heroes.thelazy.net/images/3/30/Town_portrait_Rampart_small.png',
        heroes : {
            Clancy  : {
                img      : 'https://heroes.thelazy.net/images/6/62/Hero_Clancy.png',
                type     : 'Ranger',
                specialty: 'Unicorns',
                skills   : ['Basic_Interference', 'Basic_Pathfinding'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0k'
            },
            Ivor    : {
                img      : 'https://heroes.thelazy.net/images/2/2c/Hero_Ivor.png',
                type     : 'Ranger',
                specialty: 'Elves',
                skills   : ['Basic_Archery', 'Basic_Offense'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '2-4', unit: 'Wood_Elf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0l'
            },
            Jenova  : {
                img      : 'https://heroes.thelazy.net/images/e/ed/Hero_Jenova.png',
                type     : 'Ranger',
                specialty: 'Gold',
                skills   : ['Advanced_Archery'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0m'
            },
            Kyrre   : {
                img      : 'https://heroes.thelazy.net/images/2/23/Hero_Kyrre.png',
                type     : 'Ranger',
                specialty: 'Logistics',
                skills   : ['Basic_Archery', 'Basic_Logistics'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0n'
            },
            Mephala : {
                img      : 'https://heroes.thelazy.net/images/8/86/Hero_Mephala.png',
                type     : 'Ranger',
                specialty: 'Armorer',
                skills   : ['Basic_Leadership', 'Basic_Armorer'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0o'
            },
            Ryland  : {
                img      : 'https://heroes.thelazy.net/images/0/06/Hero_Ryland.png',
                type     : 'Ranger',
                specialty: 'Dendroids',
                skills   : ['Basic_Leadership', 'Basic_Diplomacy'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0p'
            },
            Thorgrim: {
                img      : 'https://heroes.thelazy.net/images/c/ce/Hero_Thorgrim_%28HotA%29.png',
                type     : 'Ranger',
                specialty: 'Resistance',
                skills   : ['Advanced_Resistance'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0q'
            },
            Ufretin : {
                img      : 'https://heroes.thelazy.net/images/0/05/Hero_Ufretin.png',
                type     : 'Ranger',
                specialty: 'Dwarves',
                skills   : ['Basic_Interference', 'Basic_Luck'],
                units    : [{count: '3–5', unit: 'Dwarf'}, {count: '3–5', unit: 'Dwarf'}, {count: '3–5', unit: 'Dwarf'}],
                shortId  : 'h0r'
            },
            Gelu    : {
                img      : 'https://heroes.thelazy.net/images/e/e2/Hero_Gelu.png',
                type     : 'Ranger',
                specialty: 'Sharpshooters',
                skills   : ['Basic_Leadership', 'Basic_Archery'],
                units    : [{count: '6', unit: 'Sharpshooter'}, {count: '3', unit: 'Sharpshooter'}, {count: '1', unit: 'Sharpshooter'}],
                shortId  : 'h0s'
            },
            Giselle : {
                img      : 'https://heroes.thelazy.net/images/e/e4/Hero_Giselle.png',
                type     : 'Ranger',
                specialty: 'Interference',
                skills   : ['Advanced_Interference'],
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0t'
            },
            Aeris   : {
                img      : 'https://heroes.thelazy.net/images/7/74/Hero_Aeris.png',
                type     : 'Druid',
                specialty: 'Pegasi',
                skills   : ['Basic_Wisdom', 'Basic_Scouting'],
                spell    : 'Protection_from_Air',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0u'
            },
            Alagar  : {
                img      : 'https://heroes.thelazy.net/images/4/43/Hero_Alagar.png',
                type     : 'Druid',
                specialty: 'Ice_Bolt',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Ice_Bolt',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0v'
            },
            Coronius: {
                img      : 'https://heroes.thelazy.net/images/e/ed/Hero_Coronius_%28HotA%29.png',
                type     : 'Druid',
                specialty: 'Slayer',
                skills   : ['Basic_Wisdom', 'Basic_Scholar'],
                spell    : 'Slayer',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0w'
            },
            Elleshar: {
                img      : 'https://heroes.thelazy.net/images/b/b8/Hero_Elleshar.png',
                type     : 'Druid',
                specialty: 'Intelligence',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Curse',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0x'
            },
            Gem     : {
                img      : 'https://heroes.thelazy.net/images/9/9f/Hero_Gem_%28HotA%29.png',
                type     : 'Druid',
                specialty: 'First_Aid',
                skills   : ['Basic_Wisdom', 'Basic_First_Aid'],
                spell    : 'Summon_Boat',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0y'
            },
            Malcom  : {
                img      : 'https://heroes.thelazy.net/images/2/22/Hero_Malcom.png',
                type     : 'Druid',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Magic_Arrow',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h0z'
            },
            Melodia : {
                img      : 'https://heroes.thelazy.net/images/4/4b/Hero_Melodia.png',
                type     : 'Druid',
                specialty: 'Fortune',
                skills   : ['Basic_Wisdom', 'Basic_Luck'],
                spell    : 'Fortune',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h10'
            },
            Uland   : {
                img      : 'https://heroes.thelazy.net/images/e/ee/Hero_Uland.png',
                type     : 'Druid',
                specialty: 'Cure',
                skills   : ['Basic_Wisdom', 'Basic_Ballistics'],
                spell    : 'Cure',
                units    : [{count: '12–24', unit: 'Centaur'}, {count: '3–5', unit: 'Dwarf'}, {count: '2-4', unit: 'Wood_Elf'}],
                shortId  : 'h11'
            }
        },
        shortId: 't01'
    },
    Tower     : {
        img    : 'https://heroes.thelazy.net/images/1/1d/Town_portrait_Tower_small.png',
        heroes : {
            Fafner   : {
                img      : 'https://heroes.thelazy.net/images/d/d0/Hero_Fafner.png',
                type     : 'Alchemist',
                specialty: 'Nagas',
                skills   : ['Basic_Scholar', 'Basic_Interference'],
                spell    : 'Haste',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h12'
            },
            Iona     : {
                img      : 'https://heroes.thelazy.net/images/5/5c/Hero_Iona_%28HotA%29.png',
                type     : 'Alchemist',
                specialty: 'Genies',
                skills   : ['Basic_Scholar', 'Basic_Intelligence'],
                spell    : 'Magic_Arrow',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h13'
            },
            Josephine: {
                img      : 'https://heroes.thelazy.net/images/6/62/Hero_Josephine.png',
                type     : 'Alchemist',
                specialty: 'Golems',
                skills   : ['Basic_Mysticism', 'Basic_Sorcery'],
                spell    : 'Haste',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '4-5', unit: 'Stone_Golem'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h14'
            },
            Neela    : {
                img      : 'https://heroes.thelazy.net/images/8/80/Hero_Neela.png',
                type     : 'Alchemist',
                specialty: 'Armorer',
                skills   : ['Basic_Scholar', 'Basic_Armorer'],
                spell    : 'Shield',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h15'
            },
            Piquedram: {
                img      : 'https://heroes.thelazy.net/images/4/42/Hero_Piquedram.png',
                type     : 'Alchemist',
                specialty: 'Gargoyles',
                skills   : ['Basic_Mysticism', 'Basic_Scouting'],
                spell    : 'Shield',
                units    : [{count: '5-7', unit: 'Stone_Gargoyle'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '5-7', unit: 'Stone_Gargoyle'}],
                shortId  : 'h16'
            },
            Rissa    : {
                img      : 'https://heroes.thelazy.net/images/7/7f/Hero_Rissa.png',
                type     : 'Alchemist',
                specialty: 'Mercury',
                skills   : ['Basic_Mysticism', 'Basic_Offense'],
                spell    : 'Magic_Arrow',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h17'
            },
            Thane    : {
                img      : 'https://heroes.thelazy.net/images/d/d6/Hero_Thane.png',
                type     : 'Alchemist',
                specialty: 'Genies',
                skills   : ['Advanced_Scholar'],
                spell    : 'Magic_Arrow',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h18'
            },
            Torosar  : {
                img      : 'https://heroes.thelazy.net/images/b/b3/Hero_Torosar.png',
                type     : 'Alchemist',
                specialty: 'Ballista',
                skills   : ['Basic_Mysticism', 'Basic_Tactics'],
                spell    : 'Magic_Arrow',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h19'
            },
            Aine     : {
                img      : 'https://heroes.thelazy.net/images/c/cc/Hero_Aine.png',
                type     : 'Wizard',
                specialty: 'Gold',
                skills   : ['Basic_Wisdom', 'Basic_Scholar'],
                spell    : 'Curse',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1a'
            },
            Astral   : {
                img      : 'https://heroes.thelazy.net/images/3/3a/Hero_Astral.png',
                type     : 'Wizard',
                specialty: 'Hypnotize',
                skills   : ['Advanced_Wisdom'],
                spell    : 'Hypnotize',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1b'
            },
            Cyra     : {
                img      : 'https://heroes.thelazy.net/images/4/45/Hero_Cyra.png',
                type     : 'Wizard',
                specialty: 'Haste',
                skills   : ['Basic_Wisdom', 'Basic_Diplomacy'],
                spell    : 'Haste',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1c'
            },
            Daremyth : {
                img      : 'https://heroes.thelazy.net/images/0/0d/Hero_Daremyth.png',
                type     : 'Wizard',
                specialty: 'Fortune',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Fortune',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1d'
            },
            Halon    : {
                img      : 'https://heroes.thelazy.net/images/e/ec/Hero_Halon.png',
                type     : 'Wizard',
                specialty: 'Mysticism',
                skills   : ['Basic_Wisdom', 'Basic_Mysticism'],
                spell    : 'Stone_Skin',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1e'
            },
            Serena   : {
                img      : 'https://heroes.thelazy.net/images/7/77/Hero_Serena.png',
                type     : 'Wizard',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Dispel',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1f'
            },
            Solmyr   : {
                img      : 'https://heroes.thelazy.net/images/9/96/Hero_Solmyr.png',
                type     : 'Wizard',
                specialty: 'Chain_Lightning',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Chain_Lightning',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1g'
            },
            Theodorus: {
                img      : 'https://heroes.thelazy.net/images/9/99/Hero_Theodorus.png',
                type     : 'Wizard',
                specialty: 'Magi',
                skills   : ['Basic_Wisdom', 'Basic_Ballistics'],
                spell    : 'Shield',
                units    : [{count: '30–40', unit: 'Gremlin'}, {count: '5-7', unit: 'Stone_Gargoyle'}, {count: '4-5', unit: 'Stone_Golem'}],
                shortId  : 'h1h'
            },
            Dracon   : {
                img      : 'https://heroes.thelazy.net/images/c/c6/Hero_Dracon.png',
                type     : 'Wizard',
                specialty: 'Enchanters',
                skills   : ['Advanced_Wisdom'],
                spell    : 'Haste',
                units    : [{count: '6', unit: 'Enchanter'}, {count: '3', unit: 'Enchanter'}, {count: '1', unit: 'Enchanter'}],
                shortId  : 'h1i'
            }
        },
        shortId: 't02'
    },
    Inferno   : {
        img    : 'https://heroes.thelazy.net/images/0/05/Town_portrait_Inferno_small.png',
        heroes : {
            Calh    : {
                img      : 'https://heroes.thelazy.net/images/7/72/Hero_Calh.png',
                type     : 'Demoniac',
                specialty: 'Gogs',
                skills   : ['Basic_Archery', 'Basic_Scouting'],
                units    : [{count: '4–7', unit: 'Gog'}, {count: '4–7', unit: 'Gog'}, {count: '4–7', unit: 'Gog'}],
                shortId  : 'h1j'
            },
            Fiona   : {
                img      : 'https://heroes.thelazy.net/images/6/60/Hero_Fiona.png',
                type     : 'Demoniac',
                specialty: 'Hell_Hounds',
                skills   : ['Advanced_Scouting'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '3–4', unit: 'Hell_Hound'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1k'
            },
            Ignatius: {
                img      : 'https://heroes.thelazy.net/images/8/8d/Hero_Ignatius.png',
                type     : 'Demoniac',
                specialty: 'Imps',
                skills   : ['Basic_Tactics', 'Basic_Interference'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '20-30', unit: 'Imp'}, {count: '20-30', unit: 'Imp'}],
                shortId  : 'h1l'
            },
            Marius  : {
                img      : 'https://heroes.thelazy.net/images/9/92/Hero_Marius_%28HotA%29.png',
                type     : 'Demoniac',
                specialty: 'Demons',
                skills   : ['Advanced_Armorer'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1m'
            },
            Nymus   : {
                img      : 'https://heroes.thelazy.net/images/d/d2/Hero_Nymus_%28HotA%29.png',
                type     : 'Demoniac',
                specialty: 'Pit_Fiends',
                skills   : ['Advanced_Offense'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1n'
            },
            Octavia : {
                img      : 'https://heroes.thelazy.net/images/2/2a/Hero_Octavia.png',
                type     : 'Demoniac',
                specialty: 'Gold',
                skills   : ['Basic_Scholar', 'Basic_Offense'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1o'
            },
            Pyre    : {
                img      : 'https://heroes.thelazy.net/images/1/16/Hero_Pyre.png',
                type     : 'Demoniac',
                specialty: 'Ballista',
                skills   : ['Basic_Logistics', 'Basic_Artillery'],
                units    : [{count: '20–30', unit: 'Imp'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1p'
            },
            Rashka  : {
                img      : 'https://heroes.thelazy.net/images/9/93/Hero_Rashka.png',
                type     : 'Demoniac',
                specialty: 'Efreet',
                skills   : ['Basic_Scholar', 'Basic_Wisdom'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1q'
            },
            Xeron   : {
                img      : 'https://heroes.thelazy.net/images/8/89/Hero_Xeron.png',
                type     : 'Demoniac',
                specialty: 'Devils',
                skills   : ['Basic_Leadership', 'Basic_Tactics'],
                units    : [{count: '20-30', unit: 'Imp'}, {count: '3–4', unit: 'Hell_Hound'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1r'
            },
            Ash     : {
                img      : 'https://heroes.thelazy.net/images/a/a2/Hero_Ash.png',
                type     : 'Heretic',
                specialty: 'Bloodlust',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Bloodlust',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1s'
            },
            Axsis   : {
                img      : 'https://heroes.thelazy.net/images/f/f2/Hero_Axsis.png',
                type     : 'Heretic',
                specialty: 'Mysticism',
                skills   : ['Basic_Wisdom', 'Basic_Mysticism'],
                spell    : 'Protection_from_Air',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1t'
            },
            Ayden   : {
                img      : 'https://heroes.thelazy.net/images/3/36/Hero_Ayden.png',
                type     : 'Heretic',
                specialty: 'Intelligence',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'View_Earth',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1u'
            },
            Calid   : {
                img      : 'https://heroes.thelazy.net/images/c/cf/Hero_Calid.png',
                type     : 'Heretic',
                specialty: 'Sulfur',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Haste',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1v'
            },
            Olema   : {
                img      : 'https://heroes.thelazy.net/images/e/e7/Hero_Olema_%28HotA%29.png',
                type     : 'Heretic',
                specialty: 'Weakness',
                skills   : ['Basic_Wisdom', 'Basic_Ballistics'],
                spell    : 'Weakness',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1w'
            },
            Xarfax  : {
                img      : 'https://heroes.thelazy.net/images/4/40/Hero_Xarfax.png',
                type     : 'Heretic',
                specialty: 'Fireball',
                skills   : ['Basic_Wisdom', 'Basic_Leadership'],
                spell    : 'Fireball',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1x'
            },
            Xyron   : {
                img      : 'https://heroes.thelazy.net/images/3/3b/Hero_Xyron.png',
                type     : 'Heretic',
                specialty: 'Inferno_(spell)',
                skills   : ['Basic_Wisdom', 'Basic_Scholar'],
                spell    : 'Inferno_(spell)',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1y'
            },
            Zydar   : {
                img      : 'https://heroes.thelazy.net/images/9/98/Hero_Zydar.png',
                type     : 'Heretic',
                specialty: 'Sorcery',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Stone_Skin',
                units    : [{count: '20-30', unit: 'Imp'}, {count: '4–7', unit: 'Gog'}, {count: '3–4', unit: 'Hell_Hound'}],
                shortId  : 'h1z'
            }
        },
        shortId: 't03'
    },
    Necropolis: {
        img    : 'https://heroes.thelazy.net/images/5/56/Town_portrait_Necropolis_small.png',
        heroes : {
            Charna                     : {
                img      : 'https://heroes.thelazy.net/images/a/a6/Hero_Charna.png',
                type     : 'Death_Knight',
                specialty: 'Wights',
                skills   : ['Basic_Necromancy', 'Basic_Tactics'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Wight'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h20'
            },
            Clavius                    : {
                img      : 'https://heroes.thelazy.net/images/6/61/Hero_Clavius.png',
                type     : 'Death_Knight',
                specialty: 'Gold',
                skills   : ['Basic_Necromancy', 'Basic_Offense'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h21'
            },
            Galthran                   : {
                img      : 'https://heroes.thelazy.net/images/7/7e/Hero_Galthran_%28HotA%29.png',
                type     : 'Death_Knight',
                specialty: 'Skeletons',
                skills   : ['Basic_Necromancy', 'Basic_Armorer'],
                spell    : 'Shield',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '20–30', unit: 'Skeleton'}, {count: '20–30', unit: 'Skeleton'}],
                shortId  : 'h22'
            },
            Isra                       : {
                img      : 'https://heroes.thelazy.net/images/9/91/Hero_Isra.png',
                type     : 'Death_Knight',
                specialty: 'Necromancy',
                skills   : ['Advanced_Necromancy'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h23'
            },
            Moandor                    : {
                img      : 'https://heroes.thelazy.net/images/4/41/Hero_Moandor.png',
                type     : 'Death_Knight',
                specialty: 'Liches',
                skills   : ['Basic_Necromancy', 'Basic_Learning'],
                spell    : 'Slow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h24'
            },
            Straker                    : {
                img      : 'https://heroes.thelazy.net/images/e/e9/Hero_Straker.png',
                type     : 'Death_Knight',
                specialty: 'Walking_Dead',
                skills   : ['Basic_Necromancy', 'Basic_Interference'],
                spell    : 'Haste',
                units    : [{count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Walking_Dead'}],
                shortId  : 'h25'
            },
            Tamika                     : {
                img      : 'https://heroes.thelazy.net/images/0/05/Hero_Tamika.png',
                type     : 'Death_Knight',
                specialty: 'Black_Knights',
                skills   : ['Basic_Necromancy', 'Basic_Offense'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h26'
            },
            Vokial                     : {
                img      : 'https://heroes.thelazy.net/images/4/4a/Hero_Vokial.png',
                type     : 'Death_Knight',
                specialty: 'Vampires',
                skills   : ['Basic_Necromancy', 'Basic_Artillery'],
                spell    : 'Stone_Skin',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h27'
            },
            Lord_Haart_the_Death_Knight: {
                img      : 'https://heroes.thelazy.net/images/d/d1/Hero_Haart_Lich.png',
                type     : 'Death_Knight',
                specialty: 'Black_Knights',
                skills   : ['Advanced_Necromancy'],
                spell    : 'Slow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h28'
            },
            Ranloo                     : {
                img      : 'https://heroes.thelazy.net/images/2/2f/Hero_Ranloo.png',
                type     : 'Death_Knight',
                specialty: 'Ballista',
                skills   : ['Basic_Necromancy', 'Basic_Artillery'],
                spell    : 'Haste',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h29'
            },
            Aislinn                    : {
                img      : 'https://heroes.thelazy.net/images/2/25/Hero_Aislinn.png',
                type     : 'Necromancer',
                specialty: 'Meteor_Shower',
                skills   : ['Basic_Necromancy', 'Basic_Wisdom'],
                spell    : 'Meteor_Shower',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2a'
            },
            Nagash                     : {
                img      : 'https://heroes.thelazy.net/images/6/6b/Hero_Nagash.png',
                type     : 'Necromancer',
                specialty: 'Gold',
                skills   : ['Basic_Necromancy', 'Basic_Intelligence'],
                spell    : 'Protection_from_Air',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2b'
            },
            Nimbus                     : {
                img      : 'https://heroes.thelazy.net/images/c/c8/Hero_Nimbus_%28HotA%29.png',
                type     : 'Necromancer',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Necromancy', 'Basic_Eagle_Eye'],
                spell    : 'Shield',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2c'
            },
            Sandro                     : {
                img      : 'https://heroes.thelazy.net/images/1/1a/Hero_Sandro.png',
                type     : 'Necromancer',
                specialty: 'Sorcery',
                skills   : ['Basic_Necromancy', 'Basic_Sorcery'],
                spell    : 'Slow',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2d'
            },
            Septienna                  : {
                img      : 'https://heroes.thelazy.net/images/7/72/Hero_Septienna.png',
                type     : 'Necromancer',
                specialty: 'Death_Ripple',
                skills   : ['Basic_Necromancy', 'Basic_Scholar'],
                spell    : 'Death_Ripple',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2e'
            },
            Thant                      : {
                img      : 'https://heroes.thelazy.net/images/a/a1/Hero_Thant.png',
                type     : 'Necromancer',
                specialty: 'Animate_Dead',
                skills   : ['Basic_Necromancy', 'Basic_Mysticism'],
                spell    : 'Animate_Dead',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2f'
            },
            Vidomina                   : {
                img      : 'https://heroes.thelazy.net/images/6/6a/Hero_Vidomina.png',
                type     : 'Necromancer',
                specialty: 'Necromancy',
                skills   : ['Advanced_Necromancy'],
                spell    : 'Curse',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2g'
            },
            Xsi                        : {
                img      : 'https://heroes.thelazy.net/images/8/85/Hero_Xsi.png',
                type     : 'Necromancer',
                specialty: 'Stone_Skin',
                skills   : ['Basic_Necromancy', 'Basic_Learning'],
                spell    : 'Stone_Skin',
                units    : [{count: '20–30', unit: 'Skeleton'}, {count: '4–6', unit: 'Walking_Dead'}, {count: '4–6', unit: 'Wight'}],
                shortId  : 'h2h'
            }
        },
        shortId: 't04'
    },
    Dungeon   : {
        img    : 'https://heroes.thelazy.net/images/b/b3/Town_portrait_Dungeon_small.png',
        heroes : {
            Ajit        : {
                img      : 'https://heroes.thelazy.net/images/c/c9/Hero_Ajit_%28HotA%29.png',
                type     : 'Overlord',
                specialty: 'Beholders',
                skills   : ['Basic_Leadership', 'Basic_Interference'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '3–4', unit: 'Beholder'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2i'
            },
            Arlach      : {
                img      : 'https://heroes.thelazy.net/images/a/ae/Hero_Arlach.png',
                type     : 'Overlord',
                specialty: 'Ballista',
                skills   : ['Basic_Offense', 'Basic_Artillery'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2j'
            },
            Dace        : {
                img      : 'https://heroes.thelazy.net/images/a/aa/Hero_Dace.png',
                type     : 'Overlord',
                specialty: 'Minotaurs',
                skills   : ['Basic_Tactics', 'Basic_Offense'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2k'
            },
            Damacon     : {
                img      : 'https://heroes.thelazy.net/images/5/5b/Hero_Damacon.png',
                type     : 'Overlord',
                specialty: 'Gold',
                skills   : ['Advanced_Offense'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2l'
            },
            Gunnar      : {
                img      : 'https://heroes.thelazy.net/images/a/af/Hero_Gunnar_%28HotA%29.png',
                type     : 'Overlord',
                specialty: 'Logistics',
                skills   : ['Basic_Tactics', 'Basic_Logistics'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2m'
            },
            Lorelei     : {
                img      : 'https://heroes.thelazy.net/images/e/ea/Hero_Lorelei.png',
                type     : 'Overlord',
                specialty: 'Harpies',
                skills   : ['Basic_Leadership', 'Basic_Scouting'],
                units    : [{count: '6-8', unit: 'Harpy'}, {count: '6-8', unit: 'Harpy'}, {count: '6-8', unit: 'Harpy'}],
                shortId  : 'h2n'
            },
            Shakti      : {
                img      : 'https://heroes.thelazy.net/images/1/18/Hero_Shakti.png',
                type     : 'Overlord',
                specialty: 'Troglodytes',
                skills   : ['Basic_Offense', 'Basic_Tactics'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '20-30', unit: 'Troglodyte'}, {count: '20-30', unit: 'Troglodyte'}],
                shortId  : 'h2o'
            },
            Synca       : {
                img      : 'https://heroes.thelazy.net/images/9/93/Hero_Synca.png',
                type     : 'Overlord',
                specialty: 'Manticores',
                skills   : ['Basic_Leadership', 'Basic_Scholar'],
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2p'
            },
            Mutare      : {
                img      : 'https://heroes.thelazy.net/images/6/68/Hero_Mutare.png',
                type     : 'Overlord',
                specialty: 'Dragons',
                skills   : ['Basic_Estates', 'Basic_Tactics'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2q'
            },
            Mutare_Drake: {
                img      : 'https://heroes.thelazy.net/images/e/e4/Hero_Mutare_Drake.png',
                type     : 'Overlord',
                specialty: 'Dragons',
                skills   : ['Basic_Estates', 'Basic_Tactics'],
                spell    : 'Magic_Arrow',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2r'
            },
            Alamar      : {
                img      : 'https://heroes.thelazy.net/images/5/5c/Hero_Alamar_%28HotA%29.png',
                type     : 'Warlock',
                specialty: 'Resurrection',
                skills   : ['Basic_Wisdom', 'Basic_Scholar'],
                spell    : 'Resurrection',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2s'
            },
            Darkstorn   : {
                img      : 'https://heroes.thelazy.net/images/8/87/Hero_Darkstorn.png',
                type     : 'Warlock',
                specialty: 'Stone_Skin',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Stone_Skin',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2t'
            },
            Deemer      : {
                img      : 'https://heroes.thelazy.net/images/f/fe/Hero_Deemer.png',
                type     : 'Warlock',
                specialty: 'Meteor_Shower',
                skills   : ['Basic_Wisdom', 'Basic_Scouting'],
                spell    : 'Meteor_Shower',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2u'
            },
            Geon        : {
                img      : 'https://heroes.thelazy.net/images/4/48/Hero_Geon.png',
                type     : 'Warlock',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Slow',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2v'
            },
            Jaegar      : {
                img      : 'https://heroes.thelazy.net/images/9/93/Hero_Jaegar.png',
                type     : 'Warlock',
                specialty: 'Mysticism',
                skills   : ['Basic_Wisdom', 'Basic_Mysticism'],
                spell    : 'Shield',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2w'
            },
            Jeddite     : {
                img      : 'https://heroes.thelazy.net/images/c/c9/Hero_Jeddite.png',
                type     : 'Warlock',
                specialty: 'Resurrection',
                skills   : ['Advanced_Wisdom'],
                spell    : 'Resurrection',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2x'
            },
            Malekith    : {
                img      : 'https://heroes.thelazy.net/images/5/5d/Hero_Malekith.png',
                type     : 'Warlock',
                specialty: 'Sorcery',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Bloodlust',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2y'
            },
            Sephinroth  : {
                img      : 'https://heroes.thelazy.net/images/1/1e/Hero_Sephinroth_%28HotA%29.png',
                type     : 'Warlock',
                specialty: 'Crystal',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Protection_from_Air',
                units    : [{count: '20-30', unit: 'Troglodyte'}, {count: '6-8', unit: 'Harpy'}, {count: '3–4', unit: 'Beholder'}],
                shortId  : 'h2z'
            }
        },
        shortId: 't05'
    },
    Stronghold: {
        img    : 'https://heroes.thelazy.net/images/1/1c/Town_portrait_Stronghold_small.png',
        heroes : {
            Crag_Hack: {
                img      : 'https://heroes.thelazy.net/images/7/7e/Hero_Crag_Hack.png',
                type     : 'Barbarian',
                specialty: 'Offense',
                skills   : ['Advanced_Offense'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h30'
            },
            Gretchin : {
                img      : 'https://heroes.thelazy.net/images/2/22/Hero_Gretchin_%28HotA%29.png',
                type     : 'Barbarian',
                specialty: 'Goblins',
                skills   : ['Basic_Offense', 'Basic_Pathfinding'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '15–25', unit: 'Goblin'}, {count: '15–25', unit: 'Goblin'}],
                shortId  : 'h31'
            },
            Gurnisson: {
                img      : 'https://heroes.thelazy.net/images/5/5d/Hero_Gurnisson_%28HotA%29.png',
                type     : 'Barbarian',
                specialty: 'Ballista',
                skills   : ['Basic_Offense', 'Basic_Artillery'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h32'
            },
            Jabarkas : {
                img      : 'https://heroes.thelazy.net/images/f/fb/Hero_Jabarkas.png',
                type     : 'Barbarian',
                specialty: 'Orcs',
                skills   : ['Basic_Offense', 'Basic_Archery'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '4–6', unit: 'Orc'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h33'
            },
            Krellion : {
                img      : 'https://heroes.thelazy.net/images/f/f9/Hero_Krellion.png',
                type     : 'Barbarian',
                specialty: 'Ogres',
                skills   : ['Basic_Offense', 'Basic_Interference'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h34'
            },
            Shiva    : {
                img      : 'https://heroes.thelazy.net/images/5/5d/Hero_Shiva.png',
                type     : 'Barbarian',
                specialty: 'Rocs',
                skills   : ['Basic_Offense', 'Basic_Scouting'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h35'
            },
            Tyraxor  : {
                img      : 'https://heroes.thelazy.net/images/b/b6/Hero_Tyraxor_%28HotA%29.png',
                type     : 'Barbarian',
                specialty: 'Wolf_Riders',
                skills   : ['Basic_Offense', 'Basic_Tactics'],
                units    : [{count: '5–7', unit: 'Wolf_Rider'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '5–7', unit: 'Wolf_Rider'}],
                shortId  : 'h36'
            },
            Yog      : {
                img      : 'https://heroes.thelazy.net/images/3/39/Hero_Yog.png',
                type     : 'Barbarian',
                specialty: 'Cyclops',
                skills   : ['Basic_Offense', 'Basic_Ballistics'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h37'
            },
            Boragus  : {
                img      : 'https://heroes.thelazy.net/images/7/72/Hero_Boragus.png',
                type     : 'Barbarian',
                specialty: 'Ogres',
                skills   : ['Basic_Offense', 'Basic_Tactics'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h38'
            },
            Kilgor   : {
                img      : 'https://heroes.thelazy.net/images/2/2b/Hero_Kilgor.png',
                type     : 'Barbarian',
                specialty: 'Behemoths',
                skills   : ['Advanced_Offense'],
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h39'
            },
            Dessa    : {
                img      : 'https://heroes.thelazy.net/images/c/c9/Hero_Dessa.png',
                type     : 'Battle_Mage',
                specialty: 'Logistics',
                skills   : ['Basic_Wisdom', 'Basic_Logistics'],
                spell    : 'Stone_Skin',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3a'
            },
            Gird     : {
                img      : 'https://heroes.thelazy.net/images/8/8b/Hero_Gird.png',
                type     : 'Battle_Mage',
                specialty: 'Sorcery',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Bloodlust',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3b'
            },
            Gundula  : {
                img      : 'https://heroes.thelazy.net/images/e/ea/Hero_Gundula.png',
                type     : 'Battle_Mage',
                specialty: 'Offense',
                skills   : ['Basic_Wisdom', 'Basic_Offense'],
                spell    : 'Slow',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3c'
            },
            Oris     : {
                img      : 'https://heroes.thelazy.net/images/3/3a/Hero_Oris.png',
                type     : 'Battle_Mage',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Protection_from_Air',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3d'
            },
            Saurug   : {
                img      : 'https://heroes.thelazy.net/images/d/d7/Hero_Saurug.png',
                type     : 'Battle_Mage',
                specialty: 'Gems',
                skills   : ['Basic_Wisdom', 'Basic_Interference'],
                spell    : 'Bloodlust',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3e'
            },
            Terek    : {
                img      : 'https://heroes.thelazy.net/images/3/3f/Hero_Terek.png',
                type     : 'Battle_Mage',
                specialty: 'Haste',
                skills   : ['Basic_Wisdom', 'Basic_Tactics'],
                spell    : 'Haste',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3f'
            },
            Vey      : {
                img      : 'https://heroes.thelazy.net/images/0/06/Hero_Vey_%28HotA%29.png',
                type     : 'Battle_Mage',
                specialty: 'Ogres',
                skills   : ['Basic_Wisdom', 'Basic_Leadership'],
                spell    : 'Magic_Arrow',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3g'
            },
            Zubin    : {
                img      : 'https://heroes.thelazy.net/images/1/1b/Hero_Zubin_%28HotA%29.png',
                type     : 'Battle_Mage',
                specialty: 'Precision',
                skills   : ['Basic_Wisdom', 'Basic_Artillery'],
                spell    : 'Precision',
                units    : [{count: '15–25', unit: 'Goblin'}, {count: '5–7', unit: 'Wolf_Rider'}, {count: '4–6', unit: 'Orc'}],
                shortId  : 'h3h'
            }
        },
        shortId: 't06'
    },
    Fortress  : {
        img    : 'https://heroes.thelazy.net/images/0/05/Town_portrait_Fortress_small.png',
        heroes : {
            Alkin   : {
                img      : 'https://heroes.thelazy.net/images/a/ad/Hero_Alkin.png',
                type     : 'Beastmaster',
                specialty: 'Gorgons',
                skills   : ['Basic_Armorer', 'Basic_Offense'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3i'
            },
            Broghild: {
                img      : 'https://heroes.thelazy.net/images/f/fd/Hero_Broghild.png',
                type     : 'Beastmaster',
                specialty: 'Wyverns',
                skills   : ['Basic_Armorer', 'Basic_Scouting'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3j'
            },
            Bron    : {
                img      : 'https://heroes.thelazy.net/images/8/82/Hero_Bron.png',
                type     : 'Beastmaster',
                specialty: 'Basilisks',
                skills   : ['Basic_Armorer', 'Basic_Interference'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4-7', unit: 'Basilisk'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3k'
            },
            Drakon  : {
                img      : 'https://heroes.thelazy.net/images/7/74/Hero_Drakon.png',
                type     : 'Beastmaster',
                specialty: 'Gnolls',
                skills   : ['Basic_Armorer', 'Basic_Leadership'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '15-25', unit: 'Gnoll'}, {count: '15-25', unit: 'Gnoll'}],
                shortId  : 'h3l'
            },
            Gerwulf : {
                img      : 'https://heroes.thelazy.net/images/3/39/Hero_Gerwulf.png',
                type     : 'Beastmaster',
                specialty: 'Ballista',
                skills   : ['Basic_Armorer', 'Basic_Artillery'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3m'
            },
            Korbac  : {
                img      : 'https://heroes.thelazy.net/images/6/6b/Hero_Korbac.png',
                type     : 'Beastmaster',
                specialty: 'Serpent_Flies',
                skills   : ['Basic_Armorer', 'Basic_Pathfinding'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '3-4', unit: 'Serpent_Fly'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3n'
            },
            Tazar   : {
                img      : 'https://heroes.thelazy.net/images/d/dc/Hero_Tazar.png',
                type     : 'Beastmaster',
                specialty: 'Armorer',
                skills   : ['Advanced_Armorer'],
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3o'
            },
            Wystan  : {
                img      : 'https://heroes.thelazy.net/images/3/33/Hero_Wystan.png',
                type     : 'Beastmaster',
                specialty: 'Lizardmen',
                skills   : ['Basic_Armorer', 'Basic_Archery'],
                units    : [{count: '4-7', unit: 'Lizardman'}, {count: '4-7', unit: 'Lizardman'}, {count: '4-7', unit: 'Lizardman'}],
                shortId  : 'h3p'
            },
            Andra   : {
                img      : 'https://heroes.thelazy.net/images/6/6c/Hero_Andra.png',
                type     : 'Witch',
                specialty: 'Intelligence',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Dispel',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3q'
            },
            Merist  : {
                img      : 'https://heroes.thelazy.net/images/a/a6/Hero_Merist_%28HotA%29.png',
                type     : 'Witch',
                specialty: 'Stone_Skin',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Stone_Skin',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3r'
            },
            Mirlanda: {
                img      : 'https://heroes.thelazy.net/images/f/fe/Hero_Mirlanda.png',
                type     : 'Witch',
                specialty: 'Weakness',
                skills   : ['Advanced_Wisdom'],
                spell    : 'Weakness',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3s'
            },
            Rosic   : {
                img      : 'https://heroes.thelazy.net/images/6/6f/Hero_Rosic.png',
                type     : 'Witch',
                specialty: 'Mysticism',
                skills   : ['Basic_Wisdom', 'Basic_Mysticism'],
                spell    : 'Magic_Arrow',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3t'
            },
            Styg    : {
                img      : 'https://heroes.thelazy.net/images/7/7e/Hero_Styg.png',
                type     : 'Witch',
                specialty: 'Sorcery',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Shield',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3u'
            },
            Tiva    : {
                img      : 'https://heroes.thelazy.net/images/3/3d/Hero_Tiva_%28HotA%29.png',
                type     : 'Witch',
                specialty: 'Eagle_Eye',
                skills   : ['Basic_Wisdom', 'Basic_Eagle_Eye'],
                spell    : 'Stone_Skin',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3v'
            },
            Verdish : {
                img      : 'https://heroes.thelazy.net/images/1/1a/Hero_Verdish.png',
                type     : 'Witch',
                specialty: 'First_Aid',
                skills   : ['Basic_Wisdom', 'Basic_First_Aid'],
                spell    : 'Protection_from_Fire',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3w'
            },
            Voy     : {
                img      : 'https://heroes.thelazy.net/images/b/ba/Hero_Voy.png',
                type     : 'Witch',
                specialty: 'Navigation',
                skills   : ['Basic_Wisdom', 'Basic_Navigation'],
                spell    : 'Slow',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3x'
            },
            Adrienne: {
                img      : 'https://heroes.thelazy.net/images/1/1d/Hero_Adrienne.png',
                type     : 'Witch',
                specialty: 'Fire_Magic',
                skills   : ['Basic_Wisdom', 'Expert_Fire_Magic'],
                spell    : 'Inferno_(spell)',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3y'
            },
            Kinkeria: {
                img      : 'https://heroes.thelazy.net/images/0/0f/Hero_Kinkeria.png',
                type     : 'Witch',
                specialty: 'Learning',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Slow',
                units    : [{count: '15-25', unit: 'Gnoll'}, {count: '4–7', unit: 'Lizardman'}, {count: '3-4', unit: 'Serpent_Fly'}],
                shortId  : 'h3z'
            }
        },
        shortId: 't07'
    },
    Conflux   : {
        img    : 'https://heroes.thelazy.net/images/0/07/Town_portrait_Conflux_small.png',
        heroes : {
            Erdamon: {
                img      : 'https://heroes.thelazy.net/images/3/3b/Hero_Erdamon_%28HotA%29.png',
                type     : 'Planeswalker',
                specialty: 'Earth_Elementals',
                skills   : ['Basic_Tactics', 'Basic_Estates'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h40'
            },
            Fiur   : {
                img      : 'https://heroes.thelazy.net/images/0/0e/Hero_Fiur.png',
                type     : 'Planeswalker',
                specialty: 'Fire_Elementals',
                skills   : ['Advanced_Offense'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h41'
            },
            Ignissa: {
                img      : 'https://heroes.thelazy.net/images/a/a3/Hero_Ignissa.png',
                type     : 'Planeswalker',
                specialty: 'Fire_Elementals',
                skills   : ['Basic_Offense', 'Basic_Artillery'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h42'
            },
            Kalt   : {
                img      : 'https://heroes.thelazy.net/images/c/c9/Hero_Kalt_%28HotA%29.png',
                type     : 'Planeswalker',
                specialty: 'Water_Elementals',
                skills   : ['Basic_Tactics', 'Basic_Learning'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Water_Elemental'}, {count: '1-2', unit: 'Water_Elemental'}],
                shortId  : 'h43'
            },
            Lacus  : {
                img      : 'https://heroes.thelazy.net/images/9/9f/Hero_Lacus.png',
                type     : 'Planeswalker',
                specialty: 'Water_Elementals',
                skills   : ['Advanced_Tactics'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Water_Elemental'}, {count: '1-2', unit: 'Water_Elemental'}],
                shortId  : 'h44'
            },
            Monere : {
                img      : 'https://heroes.thelazy.net/images/f/fd/Hero_Monere_%28HotA%29.png',
                type     : 'Planeswalker',
                specialty: 'Psychic_Elementals',
                skills   : ['Basic_Offense', 'Basic_Logistics'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Air_Elemental'}],
                shortId  : 'h45'
            },
            Pasis  : {
                img      : 'https://heroes.thelazy.net/images/2/2d/Hero_Pasis_%28HotA%29.png',
                type     : 'Planeswalker',
                specialty: 'Psychic_Elementals',
                skills   : ['Basic_Offense', 'Basic_Artillery'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Air_Elemental'}],
                shortId  : 'h46'
            },
            Thunar : {
                img      : 'https://heroes.thelazy.net/images/2/22/Hero_Thunar.png',
                type     : 'Planeswalker',
                specialty: 'Earth_Elementals',
                skills   : ['Basic_Tactics', 'Basic_Estates'],
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h47'
            },
            Aenain : {
                img      : 'https://heroes.thelazy.net/images/2/20/Hero_Aenain_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Disrupting_Ray',
                skills   : ['Basic_Wisdom', 'Basic_Air_Magic'],
                spell    : 'Disrupting_Ray',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h48'
            },
            Brissa : {
                img      : 'https://heroes.thelazy.net/images/c/c4/Hero_Brissa_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Haste',
                skills   : ['Basic_Wisdom', 'Basic_Air_Magic'],
                spell    : 'Haste',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h49'
            },
            Ciele  : {
                img      : 'https://heroes.thelazy.net/images/a/aa/Hero_Ciele_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Magic_Arrow',
                skills   : ['Basic_Wisdom', 'Basic_Water_Magic'],
                spell    : 'Magic_Arrow',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4a'
            },
            Gelare : {
                img      : 'https://heroes.thelazy.net/images/d/dd/Hero_Gelare_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Gold',
                skills   : ['Basic_Wisdom', 'Basic_Water_Magic'],
                spell    : 'Dispel',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4b'
            },
            Grindan: {
                img      : 'https://heroes.thelazy.net/images/8/8d/Hero_Grindan_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Gold',
                skills   : ['Basic_Wisdom', 'Basic_Earth_Magic'],
                spell    : 'Slow',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4c'
            },
            Inteus : {
                img      : 'https://heroes.thelazy.net/images/9/90/Hero_Inteus_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Bloodlust',
                skills   : ['Basic_Wisdom', 'Basic_Fire_Magic'],
                spell    : 'Bloodlust',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4d'
            },
            Labetha: {
                img      : 'https://heroes.thelazy.net/images/9/90/Hero_Labetha_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Stone_Skin',
                skills   : ['Basic_Wisdom', 'Basic_Earth_Magic'],
                spell    : 'Stone_Skin',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4e'
            },
            Luna   : {
                img      : 'https://heroes.thelazy.net/images/3/34/Hero_Luna_%28HotA%29.png',
                type     : 'Elementalist',
                specialty: 'Fire_Wall',
                skills   : ['Basic_Wisdom', 'Basic_Fire_Magic'],
                spell    : 'Fire_Wall',
                units    : [{count: '15–25', unit: 'Pixie'}, {count: '3-5', unit: 'Air_Elemental'}, {count: '2-3', unit: 'Water_Elemental'}],
                shortId  : 'h4f'
            }
        },
        shortId: 't08'
    },
    Cove      : {
        img    : 'https://heroes.thelazy.net/images/5/5d/Town_portrait_Cove_small.png',
        heroes : {
            Anabel    : {
                img      : 'https://heroes.thelazy.net/images/1/1d/Hero_Anabel.png',
                type     : 'Captain',
                specialty: 'Pirates',
                skills   : ['Basic_Offense', 'Basic_Archery'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '4-7', unit: 'Pirate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4g'
            },
            Cassiopeia: {
                img      : 'https://heroes.thelazy.net/images/d/d9/Hero_Cassiopeia.png',
                type     : 'Captain',
                specialty: 'Nymphs',
                skills   : ['Basic_Offense', 'Basic_Tactics'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '15-25', unit: 'Nymph'}, {count: '15-25', unit: 'Nymph'}],
                shortId  : 'h4h'
            },
            Corkes    : {
                img      : 'https://heroes.thelazy.net/images/c/c5/Hero_Corkes.png',
                type     : 'Captain',
                specialty: 'Offense',
                skills   : ['Basic_Offense', 'Basic_Pathfinding'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4i'
            },
            Derek     : {
                img      : 'https://heroes.thelazy.net/images/b/bc/Hero_Derek.png',
                type     : 'Captain',
                specialty: 'Crew_Mates',
                skills   : ['Basic_Offense', 'Basic_Leadership'],
                units    : [{count: '6-9', unit: 'Crew_Mate'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '6-9', unit: 'Crew_Mate'}],
                shortId  : 'h4j'
            },
            Elmore    : {
                img      : 'https://heroes.thelazy.net/images/9/9f/Hero_Elmore.png',
                type     : 'Captain',
                specialty: 'Navigation',
                skills   : ['Advanced_Navigation'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4k'
            },
            Illor     : {
                img      : 'https://heroes.thelazy.net/images/e/e9/Hero_Illor.png',
                type     : 'Captain',
                specialty: 'Stormbirds',
                skills   : ['Basic_Armorer', 'Basic_Tactics'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '3-5', unit: 'Stormbird'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4l'
            },
            Jeremy    : {
                img      : 'https://heroes.thelazy.net/images/9/9d/Hero_Jeremy.png',
                type     : 'Captain',
                specialty: 'Cannon',
                skills   : ['Basic_Offense', 'Basic_Artillery'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4m'
            },
            Leena     : {
                img      : 'https://heroes.thelazy.net/images/0/0b/Hero_Leena.png',
                type     : 'Captain',
                specialty: 'Gold',
                skills   : ['Basic_Pathfinding', 'Basic_Estates'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4n'
            },
            Miriam    : {
                img      : 'https://heroes.thelazy.net/images/1/14/Hero_Miriam.png',
                type     : 'Captain',
                specialty: 'Scouting',
                skills   : ['Basic_Logistics', 'Basic_Scouting'],
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4o'
            },
            Bidley    : {
                img      : 'https://heroes.thelazy.net/images/e/e8/Hero_Bidley.png',
                type     : 'Captain',
                specialty: 'Sea_Dogs',
                skills   : ['Advanced_Offense'],
                units    : [{count: '1-3', unit: 'Sea_Dog'}, {count: '4-7', unit: 'Corsair'}, {count: '1-3', unit: 'Sea_Dog'}],
                shortId  : 'h4p'
            },
            Tark      : {
                img      : 'https://heroes.thelazy.net/images/6/66/Hero_Tark.png',
                type     : 'Captain',
                specialty: 'Nix',
                skills   : ['Basic_Offense', 'Basic_Armorer'],
                units    : [{count: '1', unit: 'Nix'}, {count: '1', unit: 'Nix'}, {count: '1', unit: 'Nix'}],
                shortId  : 'h4q'
            },
            Andal     : {
                img      : 'https://heroes.thelazy.net/images/b/b3/Hero_Andal.png',
                type     : 'Navigator',
                specialty: 'Crystal',
                skills   : ['Basic_Wisdom', 'Basic_Pathfinding'],
                spell    : 'Slow',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4r'
            },
            Astra     : {
                img      : 'https://heroes.thelazy.net/images/d/dc/Hero_Astra.png',
                type     : 'Navigator',
                specialty: 'Cure',
                skills   : ['Basic_Wisdom', 'Basic_Luck'],
                spell    : 'Cure',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4s'
            },
            Casmetra  : {
                img      : 'https://heroes.thelazy.net/images/0/09/Hero_Casmetra.png',
                type     : 'Navigator',
                specialty: 'Sea_Witches',
                skills   : ['Basic_Wisdom', 'Basic_Water_Magic'],
                spell    : 'Dispel',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4t'
            },
            Dargem    : {
                img      : 'https://heroes.thelazy.net/images/f/f0/Hero_Dargem.png',
                type     : 'Navigator',
                specialty: 'Air_Shield',
                skills   : ['Basic_Wisdom', 'Basic_Tactics'],
                spell    : 'Air_Shield',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4u'
            },
            Eovacius  : {
                img      : 'https://heroes.thelazy.net/images/3/3b/Hero_Eovacius.png',
                type     : 'Navigator',
                specialty: 'Clone',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Clone',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4v'
            },
            Manfred   : {
                img      : 'https://heroes.thelazy.net/images/e/e4/Hero_Manfred.png',
                type     : 'Navigator',
                specialty: 'Fireball',
                skills   : ['Basic_Wisdom', 'Basic_Fire_Magic'],
                spell    : 'Fireball',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4w'
            },
            Spint     : {
                img      : 'https://heroes.thelazy.net/images/1/1a/Hero_Spint.png',
                type     : 'Navigator',
                specialty: 'Sorcery',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Bless',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4x'
            },
            Zilare    : {
                img      : 'https://heroes.thelazy.net/images/9/91/Hero_Zilare.png',
                type     : 'Navigator',
                specialty: 'Forgetfulness',
                skills   : ['Basic_Wisdom', 'Basic_Interference'],
                spell    : 'Forgetfulness',
                units    : [{count: '15-25', unit: 'Nymph'}, {count: '6-9', unit: 'Crew_Mate'}, {count: '4-7', unit: 'Pirate'}],
                shortId  : 'h4y'
            }
        },
        shortId: 't09'
    },
    Factory   : {
        img    : 'https://heroes.thelazy.net/images/b/b8/Town_portrait_Factory_small.png',
        heroes : {
            Henrietta: {
                img      : 'https://heroes.thelazy.net/images/6/62/Hero_Henrietta.png',
                type     : 'Mercenary',
                specialty: 'Halflings',
                skills   : ['Basic_Leadership', 'Basic_Luck'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '15-25', unit: 'Halfling_(Factory)'}, {count: '15-25', unit: 'Halfling_(Factory)'}],
                shortId  : 'h4z'
            },
            Sam      : {
                img      : 'https://heroes.thelazy.net/images/c/cc/Hero_Sam.png',
                type     : 'Mercenary',
                specialty: 'Mechanics',
                skills   : ['Basic_Offense', 'Basic_Tactics'],
                units    : [{count: '6-8', unit: 'Mechanic'}, {count: '6-8', unit: 'Mechanic'}, {count: '6-8', unit: 'Mechanic'}],
                shortId  : 'h4z'
            },
            Tancred  : {
                img      : 'https://heroes.thelazy.net/images/f/f1/Hero_Tancred.png',
                type     : 'Mercenary',
                specialty: 'Gunslingers',
                skills   : ['Advanced_Archery'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h50'
            },
            Melchior : {
                img      : 'https://heroes.thelazy.net/images/3/38/Hero_Melchior.png',
                type     : 'Mercenary',
                specialty: 'Diplomacy',
                skills   : ['Basic_Leadership', 'Basic_Diplomacy'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h51'
            },
            Floribert: {
                img      : 'https://heroes.thelazy.net/images/c/c5/Hero_Floribert.png',
                type     : 'Mercenary',
                specialty: 'First_Aid',
                skills   : ['Basic_Armorer', 'Basic_First_Aid'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h52'
            },
            Wynona   : {
                img      : 'https://heroes.thelazy.net/images/8/87/Hero_Wynona.png',
                type     : 'Mercenary',
                specialty: 'Scouting',
                skills   : ['Basic_Archery', 'Basic_Scouting'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h52'
            },
            Dury     : {
                img      : 'https://heroes.thelazy.net/images/d/d1/Hero_Dury.png',
                type     : 'Mercenary',
                specialty: 'Armorer',
                skills   : ['Basic_Offense', 'Basic_Armorer'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h53'
            },
            Morton   : {
                img      : 'https://heroes.thelazy.net/images/2/20/Hero_Morton.png',
                type     : 'Mercenary',
                specialty: 'Ballista',
                skills   : ['Basic_Artillery', 'Basic_Ballistics'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h54'
            },
            Tavin    : {
                img      : 'https://heroes.thelazy.net/images/7/75/Hero_Tavin.png',
                type     : 'Mercenary',
                specialty: 'Offense',
                skills   : ['Basic_Offense', 'Basic_Scouting'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h55'
            },
            Murdoch  : {
                img      : 'https://heroes.thelazy.net/images/3/3f/Hero_Murdoch.png',
                type     : 'Mercenary',
                specialty: 'Archery',
                skills   : ['Basic_Archery', 'Basic_Tactics'],
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h56'
            },
            Celestine: {
                img      : 'https://heroes.thelazy.net/images/9/9a/Hero_Celestine.png',
                type     : 'Artificer',
                specialty: 'Armadillos',
                skills   : ['Basic_Wisdom', 'Basic_Pathfinding'],
                spell    : 'Haste',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '4-6', unit: 'Armadillo'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h57'
            },
            Todd     : {
                img      : 'https://heroes.thelazy.net/images/6/6b/Hero_Todd.png',
                type     : 'Artificer',
                specialty: 'Automatons',
                skills   : ['Basic_Wisdom', 'Basic_Tactics'],
                spell    : 'Shield',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '3-5', unit: 'Automaton'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h58'
            },
            Agar     : {
                img      : 'https://heroes.thelazy.net/images/a/a0/Hero_Agar.png',
                type     : 'Artificer',
                specialty: 'Sandworms',
                skills   : ['Advanced_Wisdom'],
                spell    : 'View_Earth',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h59'
            },
            Bertram  : {
                img      : 'https://heroes.thelazy.net/images/7/79/Hero_Bertram.png',
                type     : 'Artificer',
                specialty: 'Gold',
                skills   : ['Basic_Wisdom', 'Basic_Scholar'],
                spell    : 'Magic_Arrow',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h5a'
            },
            Wrathmont: {
                img      : 'https://heroes.thelazy.net/images/b/bf/Hero_Wrathmont.png',
                type     : 'Artificer',
                specialty: 'Frenzy',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Frenzy',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h5b'
            },
            Ziph     : {
                img      : 'https://heroes.thelazy.net/images/5/5e/Hero_Ziph.png',
                type     : 'Artificer',
                specialty: 'Lightning_Bolt',
                skills   : ['Basic_Wisdom', 'Basic_Sorcery'],
                spell    : 'Lightning_Bolt',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h5c'
            },
            Victoria : {
                img      : 'https://heroes.thelazy.net/images/f/f9/Hero_Victoria.png',
                type     : 'Artificer',
                specialty: 'Land_Mine',
                skills   : ['Basic_Wisdom', 'Basic_Learning'],
                spell    : 'Land_Mine',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h5d'
            },
            Eanswythe: {
                img      : 'https://heroes.thelazy.net/images/1/1c/Hero_Eanswythe.png',
                type     : 'Artificer',
                specialty: 'Weakness',
                skills   : ['Basic_Wisdom', 'Basic_Estates'],
                spell    : 'Weakness',
                units    : [{count: '15-25', unit: 'Halfling_(Factory)'}, {count: '6-8', unit: 'Mechanic'}, {count: '4-6', unit: 'Armadillo'}],
                shortId  : 'h5e'
            },
            Frederick: {
                img      : 'https://heroes.thelazy.net/images/2/2e/Hero_Frederick.png',
                type     : 'Artificer',
                specialty: 'Automatons',
                skills   : ['Basic_Wisdom', 'Basic_Intelligence'],
                spell    : 'Haste',
                units    : [{count: '3-5', unit: 'Automaton'}, {count: '3-5', unit: 'Automaton'}, {count: '3-5', unit: 'Automaton'}],
                shortId  : 'h5f'
            },
        },
        shortId: 't0a'
    }
};

const SKILLS = {
    Basic_Offense        : {img: 'https://heroes.thelazy.net/images/c/c1/Basic_Offense.png'},
    Basic_Archery        : {img: 'https://heroes.thelazy.net/images/6/6b/Basic_Archery.png'},
    Basic_Tactics        : {img: 'https://heroes.thelazy.net/images/8/81/Basic_Tactics.png'},
    Basic_Pathfinding    : {img: 'https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png'},
    Basic_Leadership     : {img: 'https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png'},
    Advanced_Navigation  : {img: 'https://heroes.thelazy.net/images/b/bf/Advanced_Navigation.png'},
    Basic_Armorer        : {img: 'https://heroes.thelazy.net/images/9/99/Basic_Armorer.png'},
    Basic_Artillery      : {img: 'https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png'},
    Basic_Estates        : {img: 'https://heroes.thelazy.net/images/3/3a/Basic_Estates.png'},
    Basic_Logistics      : {img: 'https://heroes.thelazy.net/images/7/75/Basic_Logistics.png'},
    Basic_Scouting       : {img: 'https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png'},
    Advanced_Offense     : {img: 'https://heroes.thelazy.net/images/2/22/Advanced_Offense.png'},
    Basic_Wisdom         : {img: 'https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png'},
    Basic_Luck           : {img: 'https://heroes.thelazy.net/images/f/fc/Basic_Luck.png'},
    Basic_Water_Magic    : {img: 'https://heroes.thelazy.net/images/5/59/Basic_Water_Magic.png'},
    Basic_Intelligence   : {img: 'https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png'},
    Basic_Fire_Magic     : {img: 'https://heroes.thelazy.net/images/0/00/Basic_Fire_Magic.png'},
    Basic_Sorcery        : {img: 'https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png'},
    Basic_Interference   : {img: 'https://heroes.thelazy.net/images/6/60/Basic_Interference.png'},
    Basic_Navigation     : {img: 'https://heroes.thelazy.net/images/f/f9/Basic_Navigation.png'},
    Advanced_Leadership  : {img: 'https://heroes.thelazy.net/images/f/f8/Advanced_Leadership.png'},
    Basic_Diplomacy      : {img: 'https://heroes.thelazy.net/images/3/38/Basic_Diplomacy.png'},
    Advanced_Wisdom      : {img: 'https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png'},
    Basic_Mysticism      : {img: 'https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png'},
    Basic_Learning       : {img: 'https://heroes.thelazy.net/images/a/a4/Basic_Learning.png'},
    Basic_First_Aid      : {img: 'https://heroes.thelazy.net/images/3/30/Basic_First_Aid.png'},
    Basic_Eagle_Eye      : {img: 'https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png'},
    Advanced_Archery     : {img: 'https://heroes.thelazy.net/images/2/25/Advanced_Archery.png'},
    Advanced_Resistance  : {img: 'https://heroes.thelazy.net/images/f/f1/Advanced_Resistance.png'},
    Advanced_Interference: {img: 'https://heroes.thelazy.net/images/c/c4/Advanced_Interference.png'},
    Basic_Scholar        : {img: 'https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png'},
    Basic_Ballistics     : {img: 'https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png'},
    Advanced_Scholar     : {img: 'https://heroes.thelazy.net/images/2/24/Advanced_Scholar.png'},
    Advanced_Scouting    : {img: 'https://heroes.thelazy.net/images/0/0e/Advanced_Scouting.png'},
    Advanced_Armorer     : {img: 'https://heroes.thelazy.net/images/0/06/Advanced_Armorer.png'},
    Basic_Necromancy     : {img: 'https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png'},
    Advanced_Necromancy  : {img: 'https://heroes.thelazy.net/images/7/7f/Advanced_Necromancy.png'},
    Expert_Fire_Magic    : {img: 'https://heroes.thelazy.net/images/8/85/Expert_Fire_Magic.png'},
    Advanced_Tactics     : {img: 'https://heroes.thelazy.net/images/0/03/Advanced_Tactics.png'},
    Basic_Air_Magic      : {img: 'https://heroes.thelazy.net/images/4/44/Basic_Air_Magic.png'},
    Basic_Earth_Magic    : {img: 'https://heroes.thelazy.net/images/4/48/Basic_Earth_Magic.png'}
};

const SPECIALTIES = {
    Pirates           : {img: 'https://heroes.thelazy.net/images/1/11/Specialty_Pirates.png'},
    Nymphs            : {img: 'https://heroes.thelazy.net/images/e/e1/Specialty_Nymphs.png'},
    Offense           : {img: 'https://heroes.thelazy.net/images/b/b9/Specialty_Offense.png'},
    Crew_Mates        : {img: 'https://heroes.thelazy.net/images/b/b6/Specialty_Crew_Mates.png'},
    Navigation        : {img: 'https://heroes.thelazy.net/images/c/cc/Specialty_Navigation.png'},
    Stormbirds        : {img: 'https://heroes.thelazy.net/images/d/d4/Specialty_Stormbirds.png'},
    Cannon            : {img: 'https://heroes.thelazy.net/images/f/f8/Specialty_Cannon.png'},
    Gold              : {img: 'https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png'},
    Scouting          : {img: 'https://heroes.thelazy.net/images/e/e3/Specialty_Scouting.png'},
    Sea_Dogs          : {img: 'https://heroes.thelazy.net/images/3/3a/Specialty_Sea_Dogs.png'},
    Nix               : {img: 'https://heroes.thelazy.net/images/8/8f/Specialty_Nix.png'},
    Crystal           : {img: 'https://heroes.thelazy.net/images/6/6c/Specialty_Crystal.png'},
    Cure              : {img: 'https://heroes.thelazy.net/images/7/7b/Cure.png'},
    Sea_Witches       : {img: 'https://heroes.thelazy.net/images/c/c2/Specialty_Sea_Witches.png'},
    Air_Shield        : {img: 'https://heroes.thelazy.net/images/d/d8/Air_Shield.png'},
    Clone             : {img: 'https://heroes.thelazy.net/images/c/c9/Specialty_Clone.png'},
    Fireball          : {img: 'https://heroes.thelazy.net/images/a/ae/Fireball.png'},
    Sorcery           : {img: 'https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png'},
    Forgetfulness     : {img: 'https://heroes.thelazy.net/images/f/f8/Forgetfulness.png'},
    Ballista          : {img: 'https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png'},
    Griffins          : {img: 'https://heroes.thelazy.net/images/e/e8/Specialty_Griffins.png'},
    Archery           : {img: 'https://heroes.thelazy.net/images/0/0e/Specialty_Archery.png'},
    Swordsmen         : {img: 'https://heroes.thelazy.net/images/9/90/Specialty_Swordsmen.png'},
    Archers           : {img: 'https://heroes.thelazy.net/images/6/67/Specialty_Archers.png'},
    Cavaliers         : {img: 'https://heroes.thelazy.net/images/6/64/Specialty_Cavaliers.png'},
    Estates           : {img: 'https://heroes.thelazy.net/images/8/8f/Specialty_Estates.png'},
    Speed             : {img: 'https://heroes.thelazy.net/images/c/ca/Specialty_Speed.png'},
    Bless             : {img: 'https://heroes.thelazy.net/images/5/51/Bless.png'},
    Frost_Ring        : {img: 'https://heroes.thelazy.net/images/9/99/Specialty_Frost_Ring.png'},
    Weakness          : {img: 'https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png'},
    Monks             : {img: 'https://heroes.thelazy.net/images/f/fc/Specialty_Monks.png'},
    Prayer            : {img: 'https://heroes.thelazy.net/images/f/f9/Specialty_Prayer.png'},
    First_Aid         : {img: 'https://heroes.thelazy.net/images/7/70/Specialty_First_Aid.png'},
    Eagle_Eye         : {img: 'https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png'},
    Unicorns          : {img: 'https://heroes.thelazy.net/images/2/2e/Specialty_Unicorns.png'},
    Elves             : {img: 'https://heroes.thelazy.net/images/f/f1/Specialty_Elves.png'},
    Logistics         : {img: 'https://heroes.thelazy.net/images/8/85/Specialty_Logistics.png'},
    Armorer           : {img: 'https://heroes.thelazy.net/images/3/36/Specialty_Armorer.png'},
    Dendroids         : {img: 'https://heroes.thelazy.net/images/0/0a/Specialty_Dendroids.png'},
    Resistance        : {img: 'https://heroes.thelazy.net/images/6/68/Specialty_Resistance.png'},
    Dwarves           : {img: 'https://heroes.thelazy.net/images/a/ab/Specialty_Dwarves.png'},
    Sharpshooters     : {img: 'https://heroes.thelazy.net/images/f/fe/Specialty_Sharpshooters.png'},
    Interference      : {img: 'https://heroes.thelazy.net/images/2/2c/Specialty_Interference.png'},
    Pegasi            : {img: 'https://heroes.thelazy.net/images/5/51/Specialty_Pegasi.png'},
    Ice_Bolt          : {img: 'https://heroes.thelazy.net/images/9/97/Ice_Bolt.png'},
    Slayer            : {img: 'https://heroes.thelazy.net/images/f/f2/Specialty_Slayer.png'},
    Intelligence      : {img: 'https://heroes.thelazy.net/images/7/74/Specialty_Intelligence.png'},
    Fortune           : {img: 'https://heroes.thelazy.net/images/b/bf/Fortune.png'},
    Nagas             : {img: 'https://heroes.thelazy.net/images/2/2c/Specialty_Nagas.png'},
    Genies            : {img: 'https://heroes.thelazy.net/images/0/0c/Specialty_Genies.png'},
    Golems            : {img: 'https://heroes.thelazy.net/images/d/de/Specialty_Golems.png'},
    Gargoyles         : {img: 'https://heroes.thelazy.net/images/c/c3/Specialty_Gargoyles.png'},
    Mercury           : {img: 'https://heroes.thelazy.net/images/2/2a/Specialty_Mercury.png'},
    Hypnotize         : {img: 'https://heroes.thelazy.net/images/7/77/Specialty_Hypnotize.png'},
    Haste             : {img: 'https://heroes.thelazy.net/images/3/35/Haste.png'},
    Mysticism         : {img: 'https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png'},
    Chain_Lightning   : {img: 'https://heroes.thelazy.net/images/e/ec/Specialty_Chain_Lightning.png'},
    Magi              : {img: 'https://heroes.thelazy.net/images/2/2e/Specialty_Magi.png'},
    Enchanters        : {img: 'https://heroes.thelazy.net/images/e/e5/Specialty_Enchanters.png'},
    Gogs              : {img: 'https://heroes.thelazy.net/images/7/7e/Specialty_Gogs.png'},
    Hell_Hounds       : {img: 'https://heroes.thelazy.net/images/4/4e/Specialty_Hell_Hounds.png'},
    Imps              : {img: 'https://heroes.thelazy.net/images/5/59/Specialty_Imps.png'},
    Demons            : {img: 'https://heroes.thelazy.net/images/2/25/Specialty_Demons.png'},
    Pit_Fiends        : {img: 'https://heroes.thelazy.net/images/e/ee/Specialty_Pit_Fiends.png'},
    Efreet            : {img: 'https://heroes.thelazy.net/images/2/29/Specialty_Efreet.png'},
    Devils            : {img: 'https://heroes.thelazy.net/images/c/c4/Specialty_Devils.png'},
    Bloodlust         : {img: 'https://heroes.thelazy.net/images/1/11/Bloodlust.png'},
    Sulfur            : {img: 'https://heroes.thelazy.net/images/8/87/Specialty_Sulfur.png'},
    'Inferno_(spell)' : {img: 'https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png'},
    Wights            : {img: 'https://heroes.thelazy.net/images/2/25/Specialty_Wights.png'},
    Skeletons         : {img: 'https://heroes.thelazy.net/images/d/d2/Specialty_Skeletons.png'},
    Necromancy        : {img: 'https://heroes.thelazy.net/images/6/6f/Specialty_Necromancy.png'},
    Liches            : {img: 'https://heroes.thelazy.net/images/c/cb/Specialty_Liches.png'},
    Walking_Dead      : {img: 'https://heroes.thelazy.net/images/0/00/Specialty_Walking_Dead.png'},
    Black_Knights     : {img: 'https://heroes.thelazy.net/images/c/c5/Specialty_Black_Knights.png'},
    Vampires          : {img: 'https://heroes.thelazy.net/images/a/aa/Specialty_Vampires.png'},
    Meteor_Shower     : {img: 'https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png'},
    Death_Ripple      : {img: 'https://heroes.thelazy.net/images/3/3f/Death_Ripple.png'},
    Animate_Dead      : {img: 'https://heroes.thelazy.net/images/f/f8/Animate_Dead.png'},
    Stone_Skin        : {img: 'https://heroes.thelazy.net/images/2/2d/Stone_Skin.png'},
    Beholders         : {img: 'https://heroes.thelazy.net/images/3/31/Specialty_Beholders.png'},
    Minotaurs         : {img: 'https://heroes.thelazy.net/images/9/96/Specialty_Minotaurs.png'},
    Harpies           : {img: 'https://heroes.thelazy.net/images/a/a2/Specialty_Harpies.png'},
    Troglodytes       : {img: 'https://heroes.thelazy.net/images/8/88/Specialty_Troglodytes.png'},
    Manticores        : {img: 'https://heroes.thelazy.net/images/3/3b/Specialty_Manticores.png'},
    Dragons           : {img: 'https://heroes.thelazy.net/images/7/75/Specialty_Dragons.png'},
    Resurrection      : {img: 'https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png'},
    Goblins           : {img: 'https://heroes.thelazy.net/images/c/c9/Specialty_Goblins.png'},
    Orcs              : {img: 'https://heroes.thelazy.net/images/1/14/Specialty_Orcs.png'},
    Ogres             : {img: 'https://heroes.thelazy.net/images/9/92/Specialty_Ogres.png'},
    Rocs              : {img: 'https://heroes.thelazy.net/images/4/42/Specialty_Rocs.png'},
    Wolf_Riders       : {img: 'https://heroes.thelazy.net/images/4/48/Specialty_Wolf_Riders.png'},
    Cyclops           : {img: 'https://heroes.thelazy.net/images/d/d5/Specialty_Cyclops.png'},
    Behemoths         : {img: 'https://heroes.thelazy.net/images/3/33/Specialty_Behemoths.png'},
    Gems              : {img: 'https://heroes.thelazy.net/images/4/41/Specialty_Gems.png'},
    Precision         : {img: 'https://heroes.thelazy.net/images/a/af/Specialty_Precision.png'},
    Gorgons           : {img: 'https://heroes.thelazy.net/images/c/c1/Specialty_Gorgons.png'},
    Wyverns           : {img: 'https://heroes.thelazy.net/images/d/d5/Specialty_Wyverns.png'},
    Basilisks         : {img: 'https://heroes.thelazy.net/images/d/d0/Specialty_Basilisks.png'},
    Gnolls            : {img: 'https://heroes.thelazy.net/images/9/9e/Specialty_Gnolls.png'},
    Serpent_Flies     : {img: 'https://heroes.thelazy.net/images/6/66/Specialty_Serpent_Flies.png'},
    Lizardmen         : {img: 'https://heroes.thelazy.net/images/0/07/Specialty_Lizardmen.png'},
    Fire_Magic        : {img: 'https://heroes.thelazy.net/images/c/ce/Specialty_Fire_Magic.png'},
    Learning          : {img: 'https://heroes.thelazy.net/images/c/c6/Specialty_Learning.png'},
    Earth_Elementals  : {img: 'https://heroes.thelazy.net/images/f/fa/Specialty_Earth_Elementals.png'},
    Fire_Elementals   : {img: 'https://heroes.thelazy.net/images/8/8e/Specialty_Fire_Elementals.png'},
    Water_Elementals  : {img: 'https://heroes.thelazy.net/images/e/e5/Specialty_Water_Elementals.png'},
    Psychic_Elementals: {img: 'https://heroes.thelazy.net/images/0/0c/Specialty_Psychic_Elementals.png'},
    Disrupting_Ray    : {img: 'https://heroes.thelazy.net/images/8/85/Disrupting_Ray.png'},
    Magic_Arrow       : {img: 'https://heroes.thelazy.net/images/4/44/Magic_Arrow.png'},
    Fire_Wall         : {img: 'https://heroes.thelazy.net/images/8/8b/Fire_Wall.png'},
    Halflings         : {img: 'https://heroes.thelazy.net/images/a/a2/Specialty_Halflings.png'},
    Mechanics         : {img: 'https://heroes.thelazy.net/images/3/31/Specialty_Mechanics.png'},
    Gunslingers       : {img: 'https://heroes.thelazy.net/images/b/be/Specialty_Gunslingers.png'},
    Diplomacy         : {img: 'https://heroes.thelazy.net/images/0/09/Specialty_Diplomacy.png'},
    Armadillos        : {img: 'https://heroes.thelazy.net/images/3/3b/Specialty_Armadillos.png'},
    Automatons        : {img: 'https://heroes.thelazy.net/images/5/50/Specialty_Automatons.png'},
    Sandworms         : {img: 'https://heroes.thelazy.net/images/3/35/Specialty_Sandworms.png'},
    Frenzy            : {img: 'https://heroes.thelazy.net/images/a/a0/Specialty_Frenzy.png'},
    Lightning_Bolt    : {img: 'https://heroes.thelazy.net/images/0/05/Specialty_Lightning_Bolt.png'},
    Land_Mine         : {img: 'https://heroes.thelazy.net/images/5/59/Specialty_Land_Mine.png'}
};

const SPELLS = {
    Slow                : {img: 'https://heroes.thelazy.net/images/f/f5/Slow.png'},
    Cure                : {img: 'https://heroes.thelazy.net/images/7/7b/Cure.png'},
    Dispel              : {img: 'https://heroes.thelazy.net/images/7/7a/Dispel.png'},
    Air_Shield          : {img: 'https://heroes.thelazy.net/images/d/d8/Air_Shield.png'},
    Clone               : {img: 'https://heroes.thelazy.net/images/c/c9/Specialty_Clone.png'},
    Fireball            : {img: 'https://heroes.thelazy.net/images/a/ae/Fireball.png'},
    Bless               : {img: 'https://heroes.thelazy.net/images/5/51/Bless.png'},
    Forgetfulness       : {img: 'https://heroes.thelazy.net/images/f/f8/Forgetfulness.png'},
    Frost_Ring          : {img: 'https://heroes.thelazy.net/images/9/99/Specialty_Frost_Ring.png'},
    Weakness            : {img: 'https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png'},
    Curse               : {img: 'https://heroes.thelazy.net/images/8/85/Curse.png'},
    Prayer              : {img: 'https://heroes.thelazy.net/images/f/f9/Specialty_Prayer.png'},
    Stone_Skin          : {img: 'https://heroes.thelazy.net/images/2/2d/Stone_Skin.png'},
    Protection_from_Air : {img: 'https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png'},
    Ice_Bolt            : {img: 'https://heroes.thelazy.net/images/9/97/Ice_Bolt.png'},
    Slayer              : {img: 'https://heroes.thelazy.net/images/f/f2/Specialty_Slayer.png'},
    Summon_Boat         : {img: 'https://heroes.thelazy.net/images/b/b0/Summon_Boat.png'},
    Magic_Arrow         : {img: 'https://heroes.thelazy.net/images/4/44/Magic_Arrow.png'},
    Fortune             : {img: 'https://heroes.thelazy.net/images/b/bf/Fortune.png'},
    Haste               : {img: 'https://heroes.thelazy.net/images/3/35/Haste.png'},
    Shield              : {img: 'https://heroes.thelazy.net/images/c/cf/Shield.png'},
    Hypnotize           : {img: 'https://heroes.thelazy.net/images/7/77/Specialty_Hypnotize.png'},
    Chain_Lightning     : {img: 'https://heroes.thelazy.net/images/e/ec/Specialty_Chain_Lightning.png'},
    Bloodlust           : {img: 'https://heroes.thelazy.net/images/1/11/Bloodlust.png'},
    View_Earth          : {img: 'https://heroes.thelazy.net/images/f/f9/View_Earth.png'},
    'Inferno_(spell)'   : {img: 'https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png'},
    Meteor_Shower       : {img: 'https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png'},
    Death_Ripple        : {img: 'https://heroes.thelazy.net/images/3/3f/Death_Ripple.png'},
    Animate_Dead        : {img: 'https://heroes.thelazy.net/images/f/f8/Animate_Dead.png'},
    Resurrection        : {img: 'https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png'},
    Precision           : {img: 'https://heroes.thelazy.net/images/a/af/Specialty_Precision.png'},
    Protection_from_Fire: {img: 'https://heroes.thelazy.net/images/a/a1/Protection_from_Fire.png'},
    Disrupting_Ray      : {img: 'https://heroes.thelazy.net/images/8/85/Disrupting_Ray.png'},
    Fire_Wall           : {img: 'https://heroes.thelazy.net/images/8/8b/Fire_Wall.png'},
    Land_Mine           : {img: 'https://heroes.thelazy.net/images/3/37/Land_Mine.png'},
    Lightning_Bolt      : {img: 'https://heroes.thelazy.net/images/f/fd/Lightning_Bolt.png'},
    Frenzy              : {img: 'https://heroes.thelazy.net/images/f/f1/Frenzy.png'},
};

const ATTRIBUTES = {
    Attack   : {img: 'https://heroes.thelazy.net/images/6/6f/Attack_skill.png'},
    Defense  : {img: 'https://heroes.thelazy.net/images/7/7b/Defense_skill.png'},
    Power    : {img: 'https://heroes.thelazy.net/images/e/e0/Power_skill.png'},
    Knowledge: {img: 'https://heroes.thelazy.net/images/9/90/Knowledge_skill.png'},
};

const TYPES = {
    Knight      : {Attack: 2, Defense: 2, Power: 1, Knowledge: 1},
    Cleric      : {Attack: 1, Defense: 0, Power: 2, Knowledge: 2},
    Ranger      : {Attack: 1, Defense: 3, Power: 1, Knowledge: 1},
    Druid       : {Attack: 0, Defense: 2, Power: 1, Knowledge: 2},
    Alchemist   : {Attack: 1, Defense: 1, Power: 2, Knowledge: 2},
    Wizard      : {Attack: 0, Defense: 0, Power: 2, Knowledge: 3},
    Demoniac    : {Attack: 2, Defense: 2, Power: 1, Knowledge: 1},
    Heretic     : {Attack: 1, Defense: 1, Power: 2, Knowledge: 1},
    Death_Knight: {Attack: 1, Defense: 2, Power: 2, Knowledge: 1},
    Necromancer : {Attack: 1, Defense: 0, Power: 2, Knowledge: 2},
    Overlord    : {Attack: 2, Defense: 2, Power: 1, Knowledge: 1},
    Warlock     : {Attack: 0, Defense: 0, Power: 3, Knowledge: 2},
    Barbarian   : {Attack: 4, Defense: 0, Power: 1, Knowledge: 1},
    Battle_Mage : {Attack: 2, Defense: 1, Power: 1, Knowledge: 1},
    Beastmaster : {Attack: 0, Defense: 4, Power: 1, Knowledge: 1},
    Witch       : {Attack: 0, Defense: 1, Power: 2, Knowledge: 2},
    Planeswalker: {Attack: 3, Defense: 1, Power: 1, Knowledge: 1},
    Elementalist: {Attack: 0, Defense: 0, Power: 3, Knowledge: 3},
    Captain     : {Attack: 3, Defense: 0, Power: 2, Knowledge: 1},
    Navigator   : {Attack: 2, Defense: 0, Power: 1, Knowledge: 2},
    Mercenary   : {Attack: 3, Defense: 1, Power: 1, Knowledge: 1},
    Artificer   : {Attack: 0, Defense: 1, Power: 2, Knowledge: 2}
};

const UNITS = {
    Pikeman             : {img: 'images/units/Pikeman.png'},
    Archer              : {img: 'images/units/Archer.png'},
    Griffin             : {img: 'images/units/Griffin.png'},
    Centaur             : {img: 'images/units/Centaur.png'},
    Dwarf               : {img: 'images/units/Dwarf.png'},
    Wood_Elf            : {img: 'images/units/Wood_Elf.png'},
    Gremlin             : {img: 'images/units/Gremlin.png'},
    Stone_Gargoyle      : {img: 'images/units/Stone_Gargoyle.png'},
    Stone_Golem         : {img: 'images/units/Stone_Golem.png'},
    Imp                 : {img: 'images/units/Imp.png'},
    Gog                 : {img: 'images/units/Gog.png'},
    Hell_Hound          : {img: 'images/units/Hell_Hound.png'},
    Skeleton            : {img: 'images/units/Skeleton.png'},
    Walking_Dead        : {img: 'images/units/Walking_Dead.png'},
    Wight               : {img: 'images/units/Wight.png'},
    Troglodyte          : {img: 'images/units/Troglodyte.png'},
    Harpy               : {img: 'images/units/Harpy.png'},
    Beholder            : {img: 'images/units/Beholder.png'},
    Goblin              : {img: 'images/units/Goblin.png'},
    Wolf_Rider          : {img: 'images/units/Wolf_Rider.png'},
    Orc                 : {img: 'images/units/Orc.png'},
    Gnoll               : {img: 'images/units/Gnoll.png'},
    Lizardman           : {img: 'images/units/Lizardman.png'},
    Serpent_Fly         : {img: 'images/units/Serpent_Fly.png'},
    Basilisk            : {img: 'images/units/Basilisk.png'},
    Pixie               : {img: 'images/units/Pixie.png'},
    Air_Elemental       : {img: 'images/units/Air_Elemental.png'},
    Water_Elemental     : {img: 'images/units/Water_Elemental.png'},
    Nymph               : {img: 'images/units/Nymph.png'},
    Crew_Mate           : {img: 'images/units/Crew_Mate.png'},
    Pirate              : {img: 'images/units/Pirate.png'},
    Sea_Dog             : {img: 'images/units/Sea_Dog.png'},
    Corsair             : {img: 'images/units/Corsair.png'},
    Stormbird           : {img: 'images/units/Stormbird.png'},
    Nix                 : {img: 'images/units/Nix.png'},
    Sharpshooter        : {img: 'images/units/Sharpshooter.png'},
    Enchanter           : {img: 'images/units/Enchanter.png'},
    Automaton           : {img: 'images/units/Automaton.png'},
    'Halfling_(Factory)': {img: 'images/units/Halfling.png'},
    Mechanic            : {img: 'images/units/Mechanic.png'},
    Armadillo           : {img: 'images/units/Armadillo.png'},
};
