/**
 * Support function: cast dec integer value in 36-base number
 *
 * @param {number} number integer number to cast
 *
 * @return {string}
 */
function decToABet (number) {
    let ab = '0123456789abcdefghijklmnopqrstuvwxyz',
        left = number % ab.length, nextDigit = (number - left) / ab.length, result = '';

    do {
        result = ab[left] + result;

        left = nextDigit % ab.length;
        nextDigit = (nextDigit - left) / ab.length
    } while (left || nextDigit);

    return result;
}
