const towns = {
    "castle": {"name": "Castle", "url": "https://heroes.thelazy.net/index.php/Castle", "img": "https://heroes.thelazy.net/images/b/bf/Town_portrait_Castle_small.png"},
    "rampart": {"name": "Rampart", "url": "https://heroes.thelazy.net/index.php/Rampart", "img": "https://heroes.thelazy.net/images/3/30/Town_portrait_Rampart_small.png"},
    "tower": {"name": "Tower", "url": "https://heroes.thelazy.net/index.php/Tower", "img": "https://heroes.thelazy.net/images/1/1d/Town_portrait_Tower_small.png"},
    "inferno": {"name": "Inferno", "url": "https://heroes.thelazy.net/index.php/Inferno", "img": "https://heroes.thelazy.net/images/0/05/Town_portrait_Inferno_small.png"},
    "necropolis": {"name": "Necropolis", "url": "https://heroes.thelazy.net/index.php/Necropolis", "img": "https://heroes.thelazy.net/images/5/56/Town_portrait_Necropolis_small.png"},
    "dungeon": {"name": "Dungeon", "url": "https://heroes.thelazy.net/index.php/Dungeon", "img": "https://heroes.thelazy.net/images/b/b3/Town_portrait_Dungeon_small.png"},
    "stronghold": {"name": "Stronghold", "url": "https://heroes.thelazy.net/index.php/Stronghold", "img": "https://heroes.thelazy.net/images/1/1c/Town_portrait_Stronghold_small.png"},
    "fortress": {"name": "Fortress", "url": "https://heroes.thelazy.net/index.php/Fortress", "img": "https://heroes.thelazy.net/images/0/05/Town_portrait_Fortress_small.png"},
    "conflux": {"name": "Conflux", "url": "https://heroes.thelazy.net/index.php/Conflux", "img": "https://heroes.thelazy.net/images/0/07/Town_portrait_Conflux_small.png"},
    "cove": {"name": "Cove", "url": "https://heroes.thelazy.net/index.php/Cove", "img": "https://heroes.thelazy.net/images/5/5d/Town_portrait_Cove_small.png"},
    "factory": {"name": "Factory", "url": "https://heroes.thelazy.net/index.php/Factory", "img": "https://heroes.thelazy.net/images/b/b8/Town_portrait_Factory_small.png"}
}
