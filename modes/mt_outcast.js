'use strict';

const MTOutcast = {
    bannedTowns: null,
    bannedHeroes: [
        'Sorsha', 'Sylvia', 'Roland', 'Sanya',  // Castle
        'Kyrre', 'Malcom',  // Rampart
        'Serena',  // Tower
        'Xyron',  // Inferno
        'Vokial', 'Nimbus',  // Necropolis
        'Geon', 'Gunnar',  // Dungeon
        'Dessa', 'Oris', // Stronghold
        'Voy', 'Tiva',  // Fortress
        'Elmore', 'Illor', 'Astra',  // Cove
        'Tancred', 'Bertram', 'Todd',  // Factory
    ],
    mirror: true
};
