'use strict';

// Duel 2.9a
const DUEL = {
    bannedTowns: null,
    bannedHeroes: [
        'Sorsha', 'Catherine', 'Sylvia', 'Sir_Mullich',  // Castle
        'Giselle', 'Kyrre', 'Mephala', 'Gelu', // Rampart
        'Iona', 'Aine', 'Dracon', 'Torosar', // Tower
        'Rashka', 'Octavia', 'Xyron', // Inferno
        'Galthran', 'Lord_Haart_the_Death_Knight', // Necropolis
        'Alamar', 'Gunnar', 'Deemer', 'Jeddite', 'Lorelei', // Dungeon
        'Dessa', 'Gundula', // Stronghold
        'Voy', 'Tazar', 'Kinkeria', // Fortress
        'Brissa', 'Grindan', 'Monere',  // Conflux
        'Elmore', 'Miriam', 'Tark', // Cove
        'Bertram', 'Dury', 'Melchior', 'Tavin',  // Factory
    ],
    mirror: false
};
