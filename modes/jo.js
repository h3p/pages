'use strict';

// JO 2.97
const JO = {
    bannedTowns: null,
    bannedHeroes: [
        'Beatrice', 'Cuthbert', 'Sylvia', 'Valeska', 'Sanya', 'Sir_Mullich',  // Castle
        'Mephala', 'Kyrre', 'Coronius', 'Malcom', 'Gelu', // Rampart
        'Neela', 'Torosar', 'Iona', 'Serena', 'Aine', 'Dracon',  // Tower
        'Octavia', 'Xyron', 'Ash',  // Inferno
        'Nimbus', 'Lord_Haart_the_Death_Knight', 'Xsi',  // Necropolis
        'Gunnar', 'Alamar', 'Jeddite',  // Dungeon
        'Crag_Hack', 'Dessa', 'Gundula', 'Kilgor', 'Shiva',  // Stronghold
        'Voy', 'Oris', 'Tazar', 'Tiva',  // Fortress
        'Grindan', 'Kalt', 'Monere', // Conflux
        'Elmore', 'Cassiopeia', 'Miriam', 'Bidley', 'Tark', 'Illor',  // Cove
        'Bertram', 'Dury', 'Eanswythe', 'Henrietta', 'Tavin', 'Wynona',  // Factory
    ],
    mirror: false
};
