'use strict';

// JO 2.83
const JO_283 = {
    bannedTowns: null,
    bannedHeroes: [
        'Beatrice', 'Cuthbert', 'Sylvia', 'Valeska', 'Sanya', 'Sir_Mullich',  // Castle
        'Giselle', 'Mephala', 'Ivor', 'Kyrre', 'Coronius', 'Malcom', 'Gelu', // Rampart
        'Neela', 'Torosar', 'Iona', 'Serena', 'Cyra', 'Aine', 'Dracon',  // Tower
        'Rashka', 'Octavia', 'Calh', 'Xyron', 'Ash',  // Inferno
        'Galthran', 'Nimbus', 'Thant', 'Lord_Haart_the_Death_Knight',  // Necropolis
        'Gunnar', 'Alamar', 'Jeddite',  // Dungeon
        'Crag_Hack', 'Tyraxor', 'Dessa', 'Gundula', 'Kilgor',  // Stronghold
        'Voy', 'Oris', 'Drakon', 'Wystan', 'Tazar', 'Tiva',  // Fortress
        'Brissa', 'Grindan', 'Kalt', // Conflux
        'Elmore', 'Corkes', 'Derek', 'Anabel', 'Cassiopeia', 'Miriam', 'Bidley', 'Tark',  // Cove
        'Frederick', 'Murdoch', 'Tavin'  // Factory
    ],
    mirror: false
};
