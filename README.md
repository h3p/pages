# Heroes 3 picker

This project is intended for generating heroes for `Random Hero Trade` format for Heroes of Might and Magic 3 series.

To use Randomizer user should choose a generation mode - in two words it is a list of allowed heroes and towns. This list typically associated with random map template so all generated heroes and towns will be allowed on this template. Jebus Outcast mode is used by default.

Also, user may choose interface language in settings. There are two prebuild languages: english and russian. New languages may be added in the future. English is used as default

There are few prebuilt modes, for example for [JO](https://www.h3templates.com/templates/jebus-outcast) and [mt_outcast](https://sites.google.com/view/homm3milord/templates/mt-outcast). Classic SoD and HotA templates also presented. Popular templates may be added in the future. There is mode editor which allows user to create and edit his own modes.

There is reset button which will reset app (in current browser) to its factory settings (be careful: there is no confirmation).

For fast and easy rendering all images and heroes information were scraped from [heroes.thelazy.net](https://heroes.thelazy.net/index.php/List_of_heroes_(HotA)), thanks them for organising everything in easy-to-parse way.

User guidance available at the help page, link to the help page presented on all pages at the bottom of the page

Heroes of Might and Magic 3 and all associated images used in Randomizer are property of Ubisoft Enterprise SA

## URL Query String options

Some options of Randomizer may be passed as query string parameters in URL. If any options are passed in URL and not stored in Local Storage its values will be used instead of default values. It is good option to hand over link with query string preset options to your friend, so he could start Randomizer in same way as you.

Few rules about query string:

1. query string appends in the end of the URL and starts with `?` letter, if query string already added - you should add parameters without `?` letter
2. more than one parameter in query string should be joined with `&` letter
3. each parameter has its name and value, `=` letter must be placed between name and value

List of available query string parameters:

1. mode - mode ID, *not name*
2. lang - language ID
3. strictRoll - generation way, *key*
4. rollOnStart - roll hero pair automatically after page is loaded, *key*

Note: any unknown parameters will be read, but will not be used or affect on application

Any *key* option expect **true** or **false** value

Language IDs:

1. en - English
2. ru - Russian

Mode IDs:

1. JO - Jebus Outcast version 2.97
2. JO_283 - Jebus Outcast 2.83
3. JO_UA - Jebus Outcast Unofficial Addon
4. DUEL - Duel 2.9a
5. MT_OC - mt_Outcast
6. JR4_LC - JR4 Ludo clash
7. HotADefault - Default HotA hero and town pick
8. SODDefault - Default SoD hero and town pick

Note: you may use local user modes, but it is quite useless: they are local and can't be run on other device

For Example, add this to the URL: `?mode=JO_UA&lang=ru&strictRoll=false&rollOnStart=true` to run Randomizer on new device in Jebus Outcast Unofficial Addon mode, on russian, allow repeats and roll after page is loaded.

## Main function description

Picker will take two towns from allowed list and suggest two allowed heroes from each town. There is two ways to do it: strict and free.

### Strict way

In this way generator will prevent repeats, picker will store chosen towns and heroes to do it. After all towns are used list of used towns resets. Same thing with heroes in each town. Each mode stores its chooses separately.

### Free way

In this way generator will not prevent repeats, so you can get same town or hero in next roll. Only exception is made to prevent mirror town pick for not mirror modes. This way is used by default.

Reset button in settings also clears picker chooses for all modes.

## User bans before next roll

User may choose towns or heroes which he does not want to see in next roll. To perform this action user should press *Heroes* button and choose allowed and not used yet (for strict way only) heroes and towns by clicking them. If hero or town is semitransparent then this hero or town not allowed in this mode, green check indicates that hero or town recently was used. First click will disable hero or town for next roll, click again to undo it (do not work on used heroes in strict way).

Note: close button also resets all user bans

## Reset application

User may reset application to factory defaults from settings modal window by pressing *reset* button. Also, user may pass query string parameter with name *forceReset* and value *true* (see [URL Query String options](#url-query-string-options) section for more information about it)

## Mode editor

User may create and manage his own modes (will be available only in this browser). Press *Modes* button in Randomizer to go to the Mode editor.

When user enters the page, Mode editor will suggest to choose existing user mode or create new if there is no one modes created yet. After that user will gain ability to change mode's heroes and towns bans by clicking tiles with its images. Also, user may change mirror state and mode name by clicking Miscellaneous options button.

After mode edition is finished press Save button to store all changes for current mode in Local Storage. Leaving or closing page, creation of new mode or changing current mode will discard all changes.

User may delete current mode by pressing Delete button (be careful: there is no confirmation) - all mode's data will be removed from Local Storage. Reset button in Settings also clears all user modes.

There is no way to change system modes with help of the Mode editor - below you may find how to do it.

## Share/Import template

User can share his local custom templates with another user. Another user can import it in his own collection. This will allow different users to have same template.

To share template user should load it in the Template editor, call Share modal window and send exchange string from it to the person he wants to share the template. Exchange string is Base64 encoded minified template object - any missed letters or symbols changes can damage data in it.

To import template user should get exchange string (copy it to clipboard), open Import modal window in the Template editor, paste exchange string in the field and press Ok button. Parsed template will be loaded but not saved in local collection - user must save template after he viewed it

Users should keep in mind that any change done in the imported template will still stay local.

## Language addition

To add new language you will need to work with code a little.

1. Make a copy of any language file in *lang* directory of the project
2. Rename variable on first line
3. Translate all words, names and parses in quotes - **do not** rename any keys
4. In index HTML file find tag *script* where other language files are linked
5. Copy any of this script tags and change its src attribute to your language file route (you need to change only filename)
6. Look up for line with js code like *this.modules.trans.addLang('en', EN);*
7. Copy this line and then change params in parentheses according to variable`s name from step 2
8. Look up for line with html like *\<button value="en" class="button-select__option"\>en\</button\>*
9. Copy this line and then change tag value attribute and tag content according to variable`s name from step 2

If you do not have enough experience with this - ask for help of developers

## Mode addition (for project)

To add new mode in project you will need to work with code a little. To add mode for personal use - please, use Mode editor.

1. Make a copy of any mode file in *modes* directory of the project
2. Rename variable on first line - use only alphabet and underscore
3. If you need - add or change value of *bannedTowns*: in square brackets you may list **not allowed** towns for this mode
4. Change value of *bannedHeroes*: in square brackets you may list **not allowed** heroes for this mode
5. Mirror key specifies mirror pick type: true - always mirror, false - never mirror, null - randomly mirror (There is 35% chance that roll will be mirror type)
6. In index HTML file find tag *script* where other mode files are linked
7. Copy any of this script tags and change its src attribute to your mode file route (you need to change only filename)
8. Look up for line with code like *this.modules.modeProvider.addMode('HotADefault', null, defaultHotAGame.bannedHeroes, defaultHotAGame.bannedTowns, defaultHotAGame.mirror, true);*
9. Copy this line and then change name of variable (left from dot) in parentheses according to variable`s name from step 2
10. For literal naming of the new mode you will need to edit all language files in lang directory of the project
11. In each file find *mode* key and add inside its object copy any line, then rename key according step 9 and add literal name of new mode

If you do not have enough experience with this - ask for help of developers
